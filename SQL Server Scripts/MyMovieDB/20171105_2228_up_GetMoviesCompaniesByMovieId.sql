IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMoviesCompaniesByMovieId'
		)
BEGIN
	DROP PROCEDURE up_GetMoviesCompaniesByMovieId
END
GO

CREATE PROCEDURE [dbo].[up_GetMoviesCompaniesByMovieId] @MovieId INT
AS
BEGIN
	SELECT *
	FROM MoviesCompanies 
	WHERE MovieId = @MovieId 
	ORDER BY CompanyId 
END