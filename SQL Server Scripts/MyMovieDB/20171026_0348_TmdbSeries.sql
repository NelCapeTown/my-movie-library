BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'TmdbSeries'
		)
BEGIN
	CREATE TABLE dbo.TmdbSeries (
		SeriesID INT NOT NULL IDENTITY(1, 1)
		,TmdbId INT NOT NULL
		,Title NVARCHAR(200) NOT NULL
		,Popularity DECIMAL(18, 8) NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_TmdbSeries'
			AND [object_id] = OBJECT_ID('TmdbSeries', 'U')
		)
BEGIN
	ALTER TABLE dbo.TmdbSeries ADD CONSTRAINT PK_TmdbSeries PRIMARY KEY CLUSTERED (SeriesID)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbSeriesTmdbId'
			AND [object_id] = OBJECT_ID('TmdbSeries', 'U')
		)
BEGIN
	CREATE NONCLUSTERED INDEX IX_TmdbSeriesTmdbId ON dbo.TmdbSeries (TmdbId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbSeriesTitle'
			AND [object_id] = OBJECT_ID('TmdbSeries', 'U')
		)
BEGIN
	CREATE NONCLUSTERED INDEX IX_TmdbSeriesTitle ON dbo.TmdbSeries (
		Title
		,SeriesID
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.TmdbSeries

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT