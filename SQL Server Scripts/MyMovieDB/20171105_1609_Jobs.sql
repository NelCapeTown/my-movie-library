BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'Jobs'
		)
BEGIN
	CREATE TABLE dbo.Jobs (
		JobId INT NOT NULL IDENTITY(1, 1)
		,DepartmentId INT NOT NULL
		,JobDescription NVARCHAR(200) NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_Jobs'
			AND [object_id] = OBJECT_ID('Jobs', 'U')
		)
BEGIN
	ALTER TABLE dbo.Jobs ADD CONSTRAINT PK_Jobs PRIMARY KEY CLUSTERED (JobId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_JobsDepartmentDescription'
			AND [object_id] = OBJECT_ID('Jobs', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_JobsDepartmentDescription ON dbo.Jobs (
		DepartmentId
		,JobDescription
		,JobId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_JobsDescription'
			AND [object_id] = OBJECT_ID('Jobs', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_JobsDescription ON dbo.Jobs (
		JobDescription
		,DepartmentId
		,JobId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Departments', 'U')
			AND parent_object_id = OBJECT_ID('Jobs', 'U')
		)
BEGIN
	ALTER TABLE dbo.Jobs ADD CONSTRAINT FK_Jobs_Departments FOREIGN KEY (DepartmentId) REFERENCES dbo.Departments (DepartmentId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

ALTER TABLE dbo.Jobs

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT