IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMoviesCastByMovieId'
		)
BEGIN
	DROP PROCEDURE up_GetMoviesCastByMovieId
END
GO

CREATE PROCEDURE [dbo].[up_GetMoviesCastByMovieId] @MovieId INT
AS
BEGIN
	SELECT *
	FROM MoviesCast 
	WHERE MovieId = @MovieId 
	ORDER BY CastOrder, MoviesCastId 
END