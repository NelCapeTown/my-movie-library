BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'TmdbCompanies'
		)
BEGIN
	CREATE TABLE dbo.TmdbCompanies (
		CompanyID INT NOT NULL IDENTITY(1, 1)
		,TmdbId INT NOT NULL
		,Title NVARCHAR(200) NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_TmdbCompanies'
			AND [object_id] = OBJECT_ID('TmdbCompanies', 'U')
		)
BEGIN
	ALTER TABLE dbo.TmdbCompanies ADD CONSTRAINT PK_TmdbCompanies PRIMARY KEY CLUSTERED (CompanyID)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbCompaniesTmdbId'
			AND [object_id] = OBJECT_ID('TmdbCompanies', 'U')
		)
BEGIN
	CREATE NONCLUSTERED INDEX IX_TmdbCompaniesTmdbId ON dbo.TmdbCompanies (TmdbId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbCompaniesTitle'
			AND [object_id] = OBJECT_ID('TmdbCompanies', 'U')
		)
BEGIN
	CREATE NONCLUSTERED INDEX IX_TmdbCompaniesTitle ON dbo.TmdbCompanies (
		Title
		,CompanyID
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.TmdbCompanies

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT

