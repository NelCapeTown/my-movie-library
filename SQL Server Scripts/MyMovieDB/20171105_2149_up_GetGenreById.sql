IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetGenreById'
		)
BEGIN
	DROP PROCEDURE up_GetGenreById
END
GO

CREATE PROCEDURE [dbo].[up_GetGenreById] @GenreId INT
AS
BEGIN
	SELECT *
	FROM Genres
	WHERE GenreId = @GenreId

END