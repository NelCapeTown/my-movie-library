﻿using MyMovieLibModels.Models;
using MyMovieLibMVVM.ViewModels;
using MySqlConnection;
using MyMovieDB_SQL_Data.DataClasses;
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace MyMovieLibMVVM.Views
{
    /// <summary>
    /// Interaction logic for LocationsControl.xaml
    /// </summary>
    public partial class MyLocationsControl : UserControl
    {
        public static readonly RoutedEvent StatusBarChangedEvent = EventManager.RegisterRoutedEvent(
            "ChangeStatusText", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MyLocationsControl));

        public static readonly DependencyProperty StatusTextProperty = DependencyProperty.Register("StatusText", typeof(string), typeof(MyLocationsControl));

        public string StatusText
        {
            get { return (string)GetValue(StatusTextProperty); }
            set
            {
                SetValue(StatusTextProperty, value);
            }
        }

        public event RoutedEventHandler ChangeStatusText
        {
            add { AddHandler(StatusBarChangedEvent, value); }
            remove { RemoveHandler(StatusBarChangedEvent, value); }
        }

        void RaiseStatusBarChangeEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(MyLocationsControl.StatusBarChangedEvent);
            RaiseEvent(newEventArgs);
        }

        private void UpdateStatusBarText(string newValue)
        {
            StatusText = newValue;
            RaiseStatusBarChangeEvent();
        }

        private MyLocationsViewModel viewModel;
        private string _connectionString = "";
        private string _bufferLocation;
        private bool isAdding = false;
        private Int32 intListSelected = 0;

        public MyLocationsControl()
        {
            InitializeComponent();
            viewModel = new MyLocationsViewModel();
        }

        public MyLocationsViewModel MyLocationsVM
        {
            get { return viewModel; }
            set { viewModel = value; }
        }

        public String ConnectionString
        {
            set { _connectionString = value; }
            get { return _connectionString; }
        }

        public void RefreshLocationsCollection()
        {
            viewModel.ConnectionString = _connectionString;
            viewModel.GetLocationsCollection();
            lstLocations.ItemsSource = viewModel.MyLocationsCollection;
            DataContext = MyLocationsVM.MyLocation;
            if (viewModel.MyLocationsCollection.Count > 0)
            {
                lstLocations.SelectedIndex = 0;
                _bufferLocation = viewModel.JsonLocation;
            }
        }

        private void lstLocations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstLocations.SelectedItem != null)
            {
                viewModel.MyLocation = (MyLocationsModel)lstLocations.SelectedItem;
                _bufferLocation = viewModel.JsonLocation;
                UpdateStatusBarText("My Location : " + viewModel.MyLocation.Description);
                if (lstLocations.SelectedIndex >= 0)
                    intListSelected = lstLocations.SelectedIndex;
                DataContext = viewModel.MyLocation;
            }
            else
            {
                if (viewModel.MyLocationsCollection.Count > 0)
                    lstLocations.SelectedIndex = 0;
            }
            SetStateBrowsing();
        }

        private void SetStateEditing()
        {
            btnAddNew.Visibility = Visibility.Collapsed;
            btnDelete.Visibility = Visibility.Collapsed;
            btnSave.Visibility = Visibility.Visible;
            btnCancel.Visibility = Visibility.Visible;
            lstLocations.IsEnabled = false;
        }

        private void SetStateBrowsing()
        {
            btnAddNew.Visibility = Visibility.Visible;
            btnDelete.Visibility = Visibility.Visible;
            btnSave.Visibility = Visibility.Collapsed;
            btnCancel.Visibility = Visibility.Collapsed;
            lstLocations.IsEnabled = true;
            isAdding = false;
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            isAdding = true;
            viewModel.MyLocation = new MyLocationsModel();
            DataContext = MyLocationsVM.MyLocation;
            SetStateEditing();
            txtDescription.Focus();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to remove this location?", "Confirm Deletion", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                MyLocationsDB.DeleteMyLocation(MySqlConnection.MySqlConnection.GetSqlConnection(_connectionString), MyLocationsVM.MyLocation);
                viewModel.MyLocationsCollection.Remove(MyLocationsVM.MyLocation);

            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (isAdding)
                {
                    MyLocationsDB.InsertMyLocation(MySqlConnection.MySqlConnection.GetSqlConnection(_connectionString), MyLocationsVM.MyLocation);
                    RefreshLocationsCollection();
                    lstLocations.SelectedItem = lstLocations.Items[lstLocations.Items.Count - 1];
                    DataContext = lstLocations.SelectedItem;
                }
                else
                {
                    MyLocationsDB.UpdateMyLocation(MySqlConnection.MySqlConnection.GetSqlConnection(_connectionString), MyLocationsVM.MyLocation);
                    RefreshLocationsCollection();
                    lstLocations.SelectedItem = lstLocations.Items[intListSelected];
                    DataContext = lstLocations.SelectedItem;
                }
                SetStateBrowsing();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Saving...", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            viewModel.MyLocation = viewModel.DeserializedModel(_bufferLocation);
            //viewModel.MyLocation = (MyLocationsModel)lstLocations.SelectedItem;
            if (lstLocations.SelectedIndex >= 0)
                intListSelected = lstLocations.SelectedIndex;
            _bufferLocation = viewModel.JsonLocation;
            DataContext = viewModel.MyLocation;
            SetStateBrowsing();
        }

        private void rdoDigital_Checked(object sender, RoutedEventArgs e)
        {
            txtStorageType.Text = "D";
        }

        private void rdoPhysical_Checked(object sender, RoutedEventArgs e)
        {
            txtStorageType.Text = "P";
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            SetStateBrowsing();
        }

        private void txtDescription_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.Equals(_bufferLocation, viewModel.JsonLocation))
            {
                SetStateEditing();
            }
            else
            {
                SetStateBrowsing();
            }
        }

        private void txtComment_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.Equals(_bufferLocation, viewModel.JsonLocation))
            {
                SetStateEditing();
            }
            else
            {
                SetStateBrowsing();
            }
        }

        private void txtStorageType_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.Equals(txtStorageType.Text, "D"))
            {
                rdoDigital.IsChecked = true;
            }
            else
            {
                rdoPhysical.IsChecked = true;
            }
            if (!String.Equals(_bufferLocation, viewModel.JsonLocation))
            {
                SetStateEditing();
            }
            else
            {
                SetStateBrowsing();
            }
        }
    }
}
