﻿using MyMovieLibModels.Models;
using MySqlConnection;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public class MoviesDB
    {
        public Int64 InsertMovie(SqlConnClass sqlConnClass, MoviesModel movie)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertMovie"
                , movie.TmdbId 
                , movie.ImdbId
                , movie.Overview
                , movie.OriginalLanguage
                , movie.Title 
                , movie.OriginalTitle 
                , movie.Popularity 
                , movie.ReleaseDate 
                , movie.Budget 
                , movie.Revenue 
                , movie.Runtime 
                , movie.TagLine 
                , movie.IsAdult 
                , movie.IsVideo 
                , movie.VoteAverage 
                , movie.VoteCount);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["MovieId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }

        public Int64 InsertMovie(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MoviesModel movie)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_InsertMovie"
                , movie.TmdbId
                , movie.ImdbId
                , movie.Overview
                , movie.OriginalLanguage
                , movie.Title
                , movie.OriginalTitle
                , movie.Popularity
                , movie.ReleaseDate
                , movie.Budget
                , movie.Revenue
                , movie.Runtime
                , movie.TagLine
                , movie.IsAdult
                , movie.IsVideo
                , movie.VoteAverage
                , movie.VoteCount);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["MovieId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }

        public void UpdateMovie(SqlConnClass sqlConnClass, MoviesModel movie)
        {
            sqlConnClass.ExecuteNonQuery("up_UpdateMovie", movie.MovieId 
                , movie.TmdbId
                , movie.ImdbId
                , movie.Overview
                , movie.OriginalLanguage
                , movie.Title
                , movie.OriginalTitle
                , movie.Popularity
                , movie.ReleaseDate
                , movie.Budget
                , movie.Revenue
                , movie.Runtime
                , movie.TagLine
                , movie.IsAdult
                , movie.IsVideo
                , movie.VoteAverage
                , movie.VoteCount);
        }

        public void UpdateMovie(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MoviesModel movie)
        {
            sqlConnClass.ExecuteNonQuery(sqlTran, "up_UpdateMovie", movie.MovieId 
                , movie.TmdbId
                , movie.ImdbId
                , movie.Overview
                , movie.OriginalLanguage
                , movie.Title
                , movie.OriginalTitle
                , movie.Popularity
                , movie.ReleaseDate
                , movie.Budget
                , movie.Revenue
                , movie.Runtime
                , movie.TagLine
                , movie.IsAdult
                , movie.IsVideo
                , movie.VoteAverage
                , movie.VoteCount);
        }

        public void DeleteMovie(SqlConnClass sqlConnClass, MoviesModel movie)
        {
            sqlConnClass.ExecuteNonQuery("up_DeleteMovie"
                , movie.MovieId);
        }

        public void DeleteMovie(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MoviesModel movie)
        {
            sqlConnClass.ExecuteNonQuery(sqlTran, "up_DeleteMovie"
                , movie.MovieId);
        }

        public ObservableCollection<MoviesModel> GetMovies(SqlConnClass sqlConnClass)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetMovies");
            ObservableCollection<MoviesModel> movies = new ObservableCollection<MoviesModel>();
            while (reader.Read())
            {
                MoviesModel movie = ConvertReaderToClass(reader);
                movies.Add(movie);
            }
            reader.Close();
            return movies;
        }

        public ObservableCollection<MoviesModel> GetMovies(SqlConnClass sqlConnClass, SqlTransaction sqlTran)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_GetMovies");
            ObservableCollection<MoviesModel> movies = new ObservableCollection<MoviesModel>();
            while (reader.Read())
            {
                MoviesModel movie = ConvertReaderToClass(reader);
                movies.Add(movie);
            }
            reader.Close();
            return movies;
        }

        public MoviesModel GetMovieById(SqlConnClass sqlConnClass, Int32 movieId)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetMovieById", movieId);
            if (reader.Read())
            {
                MoviesModel movie = ConvertReaderToClass(reader);
                reader.Close();
                return movie;
            }
            else
            {
                reader.Close();
                return null;
            }
        }

        public MoviesModel GetMovieById(SqlConnClass sqlConnClass, SqlTransaction sqlTran, Int32 movieId)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_GetMovieById", movieId);
            if (reader.Read())
            {
                MoviesModel movie = ConvertReaderToClass(reader);
                reader.Close();
                return movie;
            }
            else
            {
                reader.Close();
                return null;
            }
        }

        private MoviesModel ConvertReaderToClass(SqlDataReader reader)
        {
            MoviesModel movie = new MoviesModel();
            movie.MovieId = Int32.Parse(reader["MovieId"].ToString());
            movie.TmdbId = Int32.Parse(reader["Tmdbid"].ToString());
            movie.ImdbId = reader["ImdbId"].ToString();
            movie.Overview = reader["Overview"].ToString();
            movie.OriginalLanguage = reader["OriginalLanguage"].ToString();
            movie.Title = reader["Title"].ToString();
            movie.OriginalTitle = reader["OriginalTitle"].ToString();
            movie.Popularity = double.Parse(reader["Popularity"].ToString());
            movie.ReleaseDate = DateTime.Parse(reader["ReleaseDate"].ToString());
            movie.Budget = Int32.Parse(reader["Budget"].ToString());
            movie.Revenue = Int32.Parse(reader["Revenue"].ToString());
            movie.Runtime = Int32.Parse(reader["Runtime"].ToString());
            movie.TagLine = reader["TagLine"].ToString();
            movie.IsAdult = Boolean.Parse(reader["IsAdult"].ToString());
            movie.IsVideo = Boolean.Parse(reader["IsVideo"].ToString());
            movie.VoteAverage = double.Parse(reader["VoteAverage"].ToString());
            movie.VoteCount = Int32.Parse(reader["VoteCount"].ToString());
            return movie;
        }
    }
}
