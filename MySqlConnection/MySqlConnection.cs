﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MySqlConnection
{
    public static class MySqlConnection
    {
        public static SqlConnClass GetSqlConnection(String connectionString)
        {
            SqlConnClass sqlConn = new SqlConnClass(connectionString);
            return sqlConn;
        }

        public static void CloseSqlConnection(SqlConnClass sqlConn)
        {
            sqlConn.CloseConnection();
        }
    }
}
