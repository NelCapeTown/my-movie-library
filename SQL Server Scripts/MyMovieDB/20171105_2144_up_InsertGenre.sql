IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertGenre'
		)
BEGIN
	DROP PROCEDURE up_InsertGenre
END
GO

CREATE PROCEDURE [dbo].[up_InsertGenre] @GenreId INT
, @GenreDescription NVARCHAR(200)
AS
BEGIN
	INSERT INTO Genres (GenreId, GenreDescription)
	SELECT @GenreId, @GenreDescription

END