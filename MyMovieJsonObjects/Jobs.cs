﻿using MySqlConnection;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace MyMovieJsonObjects
{
    public class Job
    {
        [JsonProperty("department")]
        public String Department { get; set; }

        [JsonProperty("job_list")]
        public List<String> Jobs { get; set; }
    }

    public class JobList
    {
        [JsonProperty("jobs")]
        public List<Job> DepartmentJobList { get; set; }
    }

    public class Jobs
    {
        private string TmdbApiKey;
        private JobList _jobList = new JobList();

        public Jobs(string tmdbApiKey)
        {
            TmdbApiKey = tmdbApiKey;
        }

        public JobList TheJobList
        {
            get { return _jobList; }
        }

        public JobList GetDepartmentJobData()
        {
            try
            {
                string tmdbUrl = "https://api.themoviedb.org/3/job/list?api_key=" + TmdbApiKey;
                var client = new RestClient(tmdbUrl);
                var request = new RestRequest(Method.GET);
                request.AddParameter("undefined", "{}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (String.Equals(response.StatusCode.ToString(), "OK"))
                {
                    _jobList = JsonConvert.DeserializeObject<JobList>(response.Content);
                    return _jobList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Jobs.GetDepartmentJobData: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                throw new Exception("Jobs.GetDepartmentJobData: " + ex.Message);
            }
        }
    }
}
