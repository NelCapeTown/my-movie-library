IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertJob'
		)
BEGIN
	DROP PROCEDURE up_InsertJob
END
GO

CREATE PROCEDURE [dbo].[up_InsertJob] @DepartmentId INT
	,@JobDescription NVARCHAR(200)
AS
BEGIN
	INSERT INTO Jobs (
		DepartmentId
		,JobDescription
		)
	SELECT @DepartmentId
		,@JobDescription

	SELECT SCOPE_IDENTITY() AS JobId
END