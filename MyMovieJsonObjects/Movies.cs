﻿using MySqlConnection;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace MyMovieJsonObjects
{

    public enum TmdbImageTypes
    {
        Backdrop, Logo, Poster, Profile, Still
    }

    public class MovieGenre
    {
        [JsonProperty("id")]
        public Int32 GenreId { get; set; }

        [JsonProperty("name")]
        public string GenreName { get; set; }
    }

    public class ProductionCompany
    {
        [JsonProperty("name")]
        public string CompanyName { get; set; }

        [JsonProperty("id")]
        public Int32 CompanyId { get; set; }
    }

    public class ProductionCountry
    {
        [JsonProperty("iso_3166_1")]
        public string Iso3166_1Code { get; set; }

        [JsonProperty("name")]
        public string CountryName { get; set; }
    }

    public class SpokenLanguage
    {
        [JsonProperty("iso_639_1")]
        public string Iso639_1Code { get; set; }

        [JsonProperty("name")]
        public string LanguageName { get; set; }
    }

    public class CastMember
    {
        [JsonProperty("cast_id")]
        public Int32 CastId { get; set; }

        [JsonProperty("character")]
        public string Character { get; set; }

        [JsonProperty("credit_id")]
        public string CreditId { get; set; }

        [JsonProperty("gender")]
        public Int32 Gender { get; set; }

        [JsonProperty("id")]
        public Int32 TmdbId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("order")]
        public Int32 Order { get; set; }

        [JsonProperty("profile_path")]
        public string ProfilePath { get; set; }

    }

    public class CrewMember
    {
        [JsonProperty("credit_id")]
        public string CreditID { get; set; }

        [JsonProperty("department")]
        public string Department { get; set; }

        [JsonProperty("gender")]
        public Int32 Gender { get; set; }

        [JsonProperty("id")]
        public Int32 TmdbId { get; set; }

        [JsonProperty("job")]
        public string Job { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("profile_path")]
        public string ProfilePath { get; set; }
    }

    public class Movie
    {
        [JsonProperty("adult")]
        public bool IsAdult { get; set; }

        [JsonProperty("backdrop_path")]
        public string BackdropPath { get; set; }

        [JsonProperty("belongs_to_collection")]
        public string BelongsToCollection { get; set; }

        [JsonProperty("budget")]
        public Int64 Budget { get; set; }

        [JsonProperty("genres")]
        public List<MovieGenre> Genres { get; set; }

        [JsonProperty("homepage")]
        public string HomePage { get; set; }

        [JsonProperty("id")]
        public Int32 TmdbId { get; set; }

        [JsonProperty("imdb_id")]
        public string ImdbId { get; set; }

        [JsonProperty("original_language")]
        public string OriginalLanguage { get; set; }

        [JsonProperty("original_title")]
        public string OriginalTitle { get; set; }

        [JsonProperty("overview")]
        public string Overview { get; set; }

        [JsonProperty("popularity")]
        public double Popularity { get; set; }

        [JsonProperty("poster_path")]
        public string PosterPath { get; set; }

        [JsonProperty("production_companies")]
        public List<ProductionCompany> ProductionCompanies { get; set; }

        [JsonProperty("production_countries")]
        public List<ProductionCountry> ProductionCountries { get; set; }

        [JsonProperty("release_date")]
        public DateTime ReleaseDate { get; set; }

        [JsonProperty("revenue")]
        public Int64 Revenue { get; set; }

        [JsonProperty("runtime")]
        public Int32 RunTime { get; set; }

        [JsonProperty("spoken_languages")]
        public List<SpokenLanguage> SpokenLanguages { get; set; }

        [JsonProperty("status")]
        public string ReleaseStatus { get; set; }

        [JsonProperty("tagline")]
        public string TagLine { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("video")]
        public bool IsVideo { get; set; }

        [JsonProperty("vote_average")]
        public double VoteAverage { get; set; }

        [JsonProperty("vote_count")]
        public Int32 VoteCount { get; set; }

    }

    public class MovieCredits
    {
        [JsonProperty("id")]
        public Int32 TmdbId { get; set; }

        [JsonProperty("cast")]
        public List<CastMember> CastMembers { get; set; }

        [JsonProperty("crew")]
        public List<CrewMember> CrewMembers { get; set; }
    }

    public class Movies
    {
        private string TmdbApiKey;
        private Movie _movie = new Movie();
        private MovieCredits _moviecredits = new MovieCredits();

        public Movies(string tmdbApiKey)
        {
            TmdbApiKey = tmdbApiKey;
        }

        public Movie GetBasicMovieData(Int32 tmdbId)
        {
            try
            {
                string tmdbUrl = "https://api.themoviedb.org/3/movie/" + tmdbId.ToString() + "?api_key=" + TmdbApiKey;
                var client = new RestClient(tmdbUrl);
                var request = new RestRequest(Method.GET);
                request.AddParameter("undefined", "{}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (String.Equals(response.StatusCode.ToString(), "OK"))
                {
                    _movie = JsonConvert.DeserializeObject<Movie>(response.Content);
                    return _movie;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Movies.GetBasicMovieData: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                throw new Exception("Movies.GetBasicMovieData: " + ex.Message);
            }
        }

        public MovieCredits GetMovieCredits(Int32 tmdbId)
        {
            try
            {
                string tmdbUrl = "https://api.themoviedb.org/3/movie/" + tmdbId.ToString() + "/credits?api_key=" + TmdbApiKey;
                var client = new RestClient(tmdbUrl);
                var request = new RestRequest(Method.GET);
                request.AddParameter("undefined", "{}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (String.Equals(response.StatusCode.ToString(), "OK"))
                {
                    _moviecredits = JsonConvert.DeserializeObject<MovieCredits>(response.Content);
                    return _moviecredits;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Movies.GetMovieCredits: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                throw new Exception("Movies.GetMovieCredits: " + ex.Message);
            }
        }

        public BitmapImage GetBitmapImage(TmdbImageTypes imageType, string basePath)
        {
            try
            {
            string imagesize = "w300";
            switch (imageType)
            {
                case TmdbImageTypes.Backdrop:
                    imagesize = "w780";
                    break;
                case TmdbImageTypes.Logo:
                    imagesize = "w300";
                    break;
                case TmdbImageTypes.Poster:
                    imagesize = "w500";
                    break;
                case TmdbImageTypes.Profile:
                    imagesize = "h632";
                    break;
                case TmdbImageTypes.Still:
                    imagesize = "w300";
                    break;
            }
            string tmdbUrl = "http://image.tmdb.org/t/p/" + imagesize + "/" + basePath;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(tmdbUrl, UriKind.Absolute);
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Movies.GetBitmapImage: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                throw new Exception("Movies.GetBitmapImage: " + ex.Message);
            }
        }
    }
}
