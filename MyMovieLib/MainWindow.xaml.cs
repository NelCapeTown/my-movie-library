﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyMovieLibMVVM.Views;
using System.ComponentModel;
using MyMovieJsonObjects;
using MyMovieLibModels.Models;
using MyMovieDB_SQL_Data.DataClasses;
using MySqlConnection;

namespace MyMovieLib
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            splashWindow = new SplashControl();
        }

        public UserControl splashWindow;

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            SplashControl splashControl = new SplashControl();
            contentMain.Content = splashControl;
        }

        private void btnMyMovies_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnMySeries_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnMyArtists_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnWishlist_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnResearchMovies_Click(object sender, RoutedEventArgs e)
        {
            FindMoviesControl findMoviesControl = new FindMoviesControl();
            findMoviesControl.ConnectionString = App._connectionString;
            findMoviesControl.TmdbApiKey = App._tmdbApiKey;
            contentMain.Content = findMoviesControl;
        }

        private void btnResearchSeries_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnResearchArtists_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnMyLocations_Click(object sender, RoutedEventArgs e)
        {
            MyLocationsControl locationsControl = new MyLocationsControl();
            
            locationsControl.ConnectionString = App._connectionString;
            locationsControl.RefreshLocationsCollection();
            locationsControl.ChangeStatusText += LocationsControl_ChangeStatusText;
            contentMain.Content = locationsControl;
        }

        private void LocationsControl_ChangeStatusText(object sender, RoutedEventArgs e)
        {
            txtStatusMain.Text = ((MyLocationsControl)sender).StatusText;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to exit?", "Confirm exit", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
                this.Close();
        }

        private void btnHideMenu_Click(object sender, RoutedEventArgs e)
        {
            contentMenuStack.Visibility = System.Windows.Visibility.Collapsed;
            btnHideMenu.Visibility = System.Windows.Visibility.Collapsed;
            btnShowMenu.Visibility = System.Windows.Visibility.Visible;
        }

        private void btnShowMenu_Click(object sender, RoutedEventArgs e)
        {
            contentMenuStack.Visibility = System.Windows.Visibility.Visible;
            btnHideMenu.Visibility = System.Windows.Visibility.Visible;
            btnShowMenu.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnLoadGenres_Click(object sender, RoutedEventArgs e)
        {
            txtStatusMain.Text = "Getting list of genres from Tmdb...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += LoadGenresWorker_DoWork;
            worker.ProgressChanged += LoadGenresWorker_ProgressChanged;
            worker.RunWorkerCompleted += LoadGenresWorker_RunWorkerCompleted;
            contentMenuStack.IsEnabled = false;
            worker.RunWorkerAsync();
        }

        private void LoadGenresWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txtStatusMain.Text = "Ready";
            contentMenuStack.IsEnabled = true;
        }

        private void LoadGenresWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                txtStatusMain.Text = (String)e.UserState;
            }
        }

        private void LoadGenresWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String progressMessage;
            GenresModel genresModel;
            Genres genres = new Genres(App._tmdbApiKey);
            GenreList genreList = new GenreList();
            genreList = genres.GetGenreData();
            if (genreList != null)
            {
                Int32 totalCount = genreList.Genres.Count;
                Int32 currentCount = 0;
                SqlConnClass sqlConn = MySqlConnection.MySqlConnection.GetSqlConnection(App._connectionString);
                foreach (MovieGenre genre in genreList.Genres)
                {
                    genresModel = GenresDB.GetGenreById(sqlConn, genre.GenreId);
                    if (genresModel == null)
                    {
                        genresModel = new GenresModel();
                        genresModel.GenreId = genre.GenreId;
                        genresModel.GenreDescription = genre.GenreName;
                        GenresDB.InsertGenre(sqlConn, genresModel);
                    }
                    currentCount++;
                    progressMessage = "Processed: " + currentCount.ToString() + " of: " + totalCount.ToString() + " genres...";
                    (sender as BackgroundWorker).ReportProgress(0, progressMessage);
                }
                sqlConn.CloseConnection();
            }
            else
            {
                MessageBox.Show("Unable to read genres from Tmdb...", "Data Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void LoadJobsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txtStatusMain.Text = "Ready";
            contentMenuStack.IsEnabled = true;
        }

        private void LoadJobsWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                txtStatusMain.Text = (String)e.UserState;
            }
        }

        private void LoadJobsWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String progressMessage;
            DepartmentsModel departmentsModel;
            JobsModel jobsModel = new JobsModel();
            Jobs jobs = new Jobs(App._tmdbApiKey);
            JobList jobList = new JobList();
            jobList = jobs.GetDepartmentJobData();
            if (jobList != null)
            {
                Int32 totalDepartmentCount = jobList.DepartmentJobList.Count;
                Int32 currentDepartmentCount = 0;
                Int32 currentJobCount = 0;
                SqlConnClass sqlConn = MySqlConnection.MySqlConnection.GetSqlConnection(App._connectionString);
                foreach (Job job in jobList.DepartmentJobList)
                {
                    departmentsModel = DepartmentsDB.GetDepartmentByDescription(sqlConn, job.Department);
                    if (departmentsModel == null)
                    {
                        departmentsModel = new DepartmentsModel();
                        departmentsModel.DepartmentDescription = job.Department;
                        Int32 departmentId = DepartmentsDB.InsertDepartment(sqlConn, departmentsModel);
                        departmentsModel.DepartmentId = departmentId;
                    }
                    currentJobCount = 0;
                    currentDepartmentCount++;
                    foreach (string jobDescription in job.Jobs)
                    {
                        jobsModel.JobDescription = jobDescription;
                        jobsModel = JobsDB.GetJobByDescription(sqlConn, jobsModel);
                        if (jobsModel == null)
                        {
                            jobsModel = new JobsModel();
                            jobsModel.DepartmentId = departmentsModel.DepartmentId;
                            jobsModel.JobDescription = jobDescription;
                            JobsDB.InsertJob(sqlConn, jobsModel);
                        }
                        currentJobCount++;
                        progressMessage = "Processed: " + currentDepartmentCount.ToString() + " of: " + totalDepartmentCount.ToString() + " departments - " + currentJobCount.ToString() + " jobs...";
                        (sender as BackgroundWorker).ReportProgress(0, progressMessage);
                    }
                }
                sqlConn.CloseConnection();
            }
            else
            {
                MessageBox.Show("Unable to read jobs from Tmdb...", "Data Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnLoadJobs_Click(object sender, RoutedEventArgs e)
        {
            txtStatusMain.Text = "Getting list of jobs from Tmdb...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += LoadJobsWorker_DoWork;
            worker.ProgressChanged += LoadJobsWorker_ProgressChanged;
            worker.RunWorkerCompleted += LoadJobsWorker_RunWorkerCompleted;
            contentMenuStack.IsEnabled = false;
            worker.RunWorkerAsync();
        }
    }
}

