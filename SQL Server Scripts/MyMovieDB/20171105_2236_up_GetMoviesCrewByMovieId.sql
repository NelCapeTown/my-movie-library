IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMoviesCrewByMovieId'
		)
BEGIN
	DROP PROCEDURE up_GetMoviesCrewByMovieId
END
GO

CREATE PROCEDURE [dbo].[up_GetMoviesCrewByMovieId] @MovieId INT
AS
BEGIN
	SELECT *
	FROM MoviesCrew 
	WHERE MovieId = @MovieId 
	ORDER BY DepartmentId, JobId, MoviesCrewId 
END
