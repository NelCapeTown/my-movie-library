IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetJobs'
		)
BEGIN
	DROP PROCEDURE up_GetJobs
END
GO

CREATE PROCEDURE [dbo].[up_GetJobs] 
AS
BEGIN
	SELECT *
	FROM Jobs
	ORDER BY JobId

END
