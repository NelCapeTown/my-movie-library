BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'MoviesCompanies'
		)
BEGIN
	CREATE TABLE dbo.MoviesCompanies (
		MovieId INT NOT NULL
		,CompanyId INT NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_MoviesCompanies'
			AND [object_id] = OBJECT_ID('MoviesCompanies', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCompanies ADD CONSTRAINT PK_MoviesCompanies PRIMARY KEY CLUSTERED (
		MovieId
		,CompanyId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Movies', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCompanies', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCompanies ADD CONSTRAINT FK_MoviesCompanies_Movies FOREIGN KEY (MovieId) REFERENCES dbo.Movies (MovieId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('TmdbCompanies', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCompanies', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCompanies ADD CONSTRAINT FK_MoviesCompanies_Companies FOREIGN KEY (CompanyId) REFERENCES dbo.TmdbCompanies (CompanyID) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

ALTER TABLE dbo.MoviesCompanies

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT