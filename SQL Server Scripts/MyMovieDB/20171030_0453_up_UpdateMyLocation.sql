IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_UpdateMyLocation'
		)
BEGIN
	DROP PROCEDURE up_UpdateMyLocation
END
GO

CREATE PROCEDURE [dbo].[up_UpdateMyLocation] @MyLocationID INT
	,@Description NVARCHAR(200)
	,@StorageType NVARCHAR(2)
	,@Comments NVARCHAR(MAX)
AS
BEGIN
	UPDATE MyLocations
	SET [Description] = @Description
		,StorageType = @StorageType
		,Comments = @Comments
	WHERE MyLocationId = @MyLocationID
END