IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetJobById'
		)
BEGIN
	DROP PROCEDURE up_GetJobById
END
GO

CREATE PROCEDURE [dbo].[up_GetJobById] @JobId INT
AS
BEGIN
	SELECT *
	FROM Jobs
	WHERE JobId = @JobId

END