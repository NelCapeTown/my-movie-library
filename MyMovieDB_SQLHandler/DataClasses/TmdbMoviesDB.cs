﻿using MyMovieLibModels.Models;
using MySqlConnection;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public class TmdbMoviesDB
    {
        public static ObservableCollection<TmdbMovieModel> SearchTmdbMovies(SqlConnClass sqlConnClass, string searchString)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_SearchTmdbMovies", searchString);
                ObservableCollection<TmdbMovieModel> tmdbMovies = new ObservableCollection<TmdbMovieModel>();
                while (reader.Read())
                {
                    TmdbMovieModel tmdbMovie = ConvertReaderToClass(reader);
                    tmdbMovies.Add(tmdbMovie);
                }
                reader.Close();
                return tmdbMovies;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: TmdbMoviesDB.SearchTmdbMovies: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: TmdbMoviesDB.SearchTmdbMovies: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: TmdbMoviesDB.SearchTmdbMovies: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: TmdbMoviesDB.SearchTmdbMovies: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private static TmdbMovieModel ConvertReaderToClass(SqlDataReader reader)
        {
            try
            {
                TmdbMovieModel tmdbMovie = new TmdbMovieModel();
                tmdbMovie.TmdbId = Int32.Parse(reader["TmdbId"].ToString());
                tmdbMovie.MovieTitle = reader["Title"].ToString();
                return tmdbMovie;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: TmdbMoviesDB.ConvertReaderToClass: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: TmdbMoviesDB.ConvertReaderToClass: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

    }
}
