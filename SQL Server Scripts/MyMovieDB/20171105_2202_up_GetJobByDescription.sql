IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetJobByDescription'
		)
BEGIN
	DROP PROCEDURE up_GetJobByDescription
END
GO

CREATE PROCEDURE [dbo].[up_GetJobByDescription] @DepartmentId INT
, @JobDescription NVARCHAR(200)
AS
BEGIN
	SELECT *
	FROM Jobs
	WHERE DepartmentId = @DepartmentId 
	AND JobDescription = @JobDescription

END
