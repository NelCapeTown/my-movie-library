IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetDepartmentById'
		)
BEGIN
	DROP PROCEDURE up_GetDepartmentById
END
GO

CREATE PROCEDURE [dbo].[up_GetDepartmentById] @DepartmentId INT
AS
BEGIN
	SELECT *
	FROM Departments
	WHERE DepartmentId = @DepartmentId

END