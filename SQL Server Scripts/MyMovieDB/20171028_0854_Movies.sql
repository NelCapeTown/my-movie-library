BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'Movies'
		)
BEGIN
	CREATE TABLE dbo.Movies (
		MovieId INT NOT NULL IDENTITY(1, 1)
		,TmdbId INT NOT NULL
		,ImdbId NVARCHAR(50) NULL
		,Overview NVARCHAR(MAX) NULL
		,OriginalLanguage NVARCHAR(30) NULL
		,Title NVARCHAR(400) NULL
		,OriginalTitle NVARCHAR(400) NULL
		,Popularity DECIMAL(18, 8) NULL
		,ReleaseDate DATE NULL
		,Budget MONEY NULL
		,Revenue MONEY NULL
		,Runtime INT NULL
		,TagLine VARCHAR(MAX) NULL
		,IsAdult BIT NOT NULL
		,IsVideo BIT NOT NULL
		,VoteAverage DECIMAL(18, 2) NULL
		,VoteCount INT NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE parent_object_id = OBJECT_ID('Movies', 'U')
			AND [name] = 'DF_Movies_IsAdult'
		)
BEGIN
	ALTER TABLE dbo.Movies ADD CONSTRAINT DF_Movies_IsAdult DEFAULT 0
	FOR IsAdult
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE parent_object_id = OBJECT_ID('Movies', 'U')
			AND [name] = 'DF_Movies_IsVideo'
		)
BEGIN
	ALTER TABLE dbo.Movies ADD CONSTRAINT DF_Movies_IsVideo DEFAULT 0
	FOR IsVideo
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_Movies'
			AND [object_id] = OBJECT_ID('Movies', 'U')
		)
BEGIN
	ALTER TABLE dbo.Movies ADD CONSTRAINT PK_Movies PRIMARY KEY CLUSTERED (MovieId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesTitle'
			AND [object_id] = OBJECT_ID('Movies', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX c ON dbo.Movies (
		Title
		,ReleaseDate
		,MovieId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesVotes'
			AND [object_id] = OBJECT_ID('Movies', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesVotes ON dbo.Movies (
		VoteAverage
		,ReleaseDate
		,MovieId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.Movies

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT