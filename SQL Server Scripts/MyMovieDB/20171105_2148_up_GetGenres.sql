IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetGenres'
		)
BEGIN
	DROP PROCEDURE up_GetGenres
END
GO

CREATE PROCEDURE [dbo].[up_GetGenres] 
AS
BEGIN
	SELECT *
	FROM Genres
	ORDER BY GenreId

END