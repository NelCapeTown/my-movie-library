BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'MoviesGenres'
		)
BEGIN
	CREATE TABLE dbo.MoviesGenres (
		MovieId INT NOT NULL
		,GenreId INT NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_MoviesGenres'
			AND [object_id] = OBJECT_ID('MoviesGenres', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesGenres ADD CONSTRAINT PK_MoviesGenres PRIMARY KEY CLUSTERED (
		MovieId
		,GenreId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Movies', 'U')
			AND parent_object_id = OBJECT_ID('MoviesGenres', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesGenres ADD CONSTRAINT FK_MoviesGenres_Movies FOREIGN KEY (MovieId) REFERENCES dbo.Movies (MovieId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Genres', 'U')
			AND parent_object_id = OBJECT_ID('MoviesGenres', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesGenres ADD CONSTRAINT FK_MoviesGenres_Genres FOREIGN KEY (GenreId) REFERENCES dbo.Genres (GenreId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

ALTER TABLE dbo.MoviesGenres

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT