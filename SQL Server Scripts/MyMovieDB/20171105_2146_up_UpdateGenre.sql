IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_UpdateGenre'
		)
BEGIN
	DROP PROCEDURE up_UpdateGenre
END
GO

CREATE PROCEDURE [dbo].[up_UpdateGenre] @GenreId INT
	,@GenreDescription NVARCHAR(200)
AS
BEGIN
	UPDATE Genres
	SET GenreDescription = @GenreDescription
	WHERE GenreId = @GenreId
END