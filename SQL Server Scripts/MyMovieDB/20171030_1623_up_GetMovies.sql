IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMovies'
		)
BEGIN
	DROP PROCEDURE up_GetMovies
END
GO

CREATE PROCEDURE [dbo].[up_GetMovies] @MovieId INT
AS
BEGIN
	SELECT *
	FROM Movies
	ORDER BY MovieId
END