﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xaml;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.IO;
using MySqlConnection;

namespace ImageHandling
{
    public class ImageProperties
    {
        private byte[] _byteArray = new byte[1];
        public string Folder { get; set; }
        public string FileName { get; set; }
        public DateTime FileCreateDate { get; set; }
        public DateTime FileModifiedDate { get; set; }
        public Int64 FileSize { get; set; }
        public Int32 ImageWidth { get; set; }
        public Int32 ImageHeight { get; set; }
        public Double Diagonal { get; set; }
        public Double AspectRatio { get; set; }
        public string Orientation { get; set; }

        public byte[] ByteArray
        {
            get
            {
                return _byteArray;
            }
        }

        public ImageProperties()
        {
            _byteArray[0] = 1;
        }

        public void AssignByteArray(byte[] SourceArray)
        {
            Array.Resize<byte>(ref _byteArray, SourceArray.Length);
            SourceArray.CopyTo(_byteArray, 0);
        }
    }

    public static class ImageFactory
    {
        //Converts and image from a BitmapImage into an array of bytes
        //The source BitmapImage must already be opened and initialised
        public static byte[] BitmapImageToByteArray(BitmapImage source)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(source));
                encoder.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                byte[] buffer = ms.ToArray();
                return buffer;
            }
            catch (NotSupportedException nsex)
            {
                ErrorClass.LogError("Not supported exception: BitmapImageToByteArray >>> BitmapImage: " + source.BaseUri.ToString() + " - Error: " + nsex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                MemoryStream ms = new MemoryStream();
                byte[] buffer = ms.ToArray();
                return buffer;
            }
            catch (OutOfMemoryException omex)
            {
                ErrorClass.LogError("Out of memory exception: BitmapImageToByteArray >>> BitmapImage: " + source.BaseUri.ToString() + " - Error: " + omex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                MemoryStream ms = new MemoryStream();
                byte[] buffer = ms.ToArray();
                return buffer;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("BitmapImageToByteArray >>> BitmapImage: " + source.BaseUri.ToString() + " - Error: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                MemoryStream ms = new MemoryStream();
                byte[] buffer = ms.ToArray();
                return buffer;
            }
        }

        public static BitmapImage ByteArrayToBitmapImage(byte[] source)
        {
            BitmapImage bitmap = new BitmapImage();
            try
            {
                MemoryStream ms = new MemoryStream(source);
                ms.Position = 0;
                bitmap.BeginInit();
                bitmap.StreamSource = ms;
                bitmap.EndInit();
                return bitmap;
            }
            catch (NotSupportedException nsex)
            {
                ErrorClass.LogError("Not supported exception: ByteArrayToBitmapImage >>> BitmapImage:  - Error: " + nsex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                return bitmap;
            }
            catch (OutOfMemoryException omex)
            {
                ErrorClass.LogError("Out of memory exception: ByteArrayToBitmapImage >>> BitmapImage:  - Error: " + omex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                return bitmap;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("ByteArrayToBitmapImage >>> BitmapImage:  - Error: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                return bitmap;
            }
        }

        public static ImageProperties GetImageProperties(string filePath, Boolean copyByteArray = true)
        {
            ImageProperties props = new ImageProperties();
            BitmapImage src = new BitmapImage();
            try
            {
                src.BeginInit();
                src.UriSource = new Uri(filePath, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                int lastPosition = filePath.LastIndexOf("\\");
                props.Folder = filePath.Substring(0, lastPosition);
                props.FileName = filePath.Substring(lastPosition + 1);
                props.FileCreateDate = File.GetCreationTime(filePath);
                props.FileModifiedDate = File.GetLastAccessTime(filePath);
                props.FileSize = new FileInfo(filePath).Length;
                props.ImageWidth = Convert.ToInt32(src.Width);
                props.ImageHeight = Convert.ToInt32(src.Height);
                props.Diagonal = Math.Sqrt(Math.Pow(src.Width, 2) + Math.Pow(src.Height, 2));
                if (props.ImageHeight != 0)
                {
                    props.AspectRatio = Convert.ToDouble(props.ImageWidth) / Convert.ToDouble(props.ImageHeight);
                }
                else
                {
                    props.AspectRatio = 1;
                }
                if (props.ImageWidth > props.ImageHeight)
                {
                    props.Orientation = "L";
                }
                else
                {
                    props.Orientation = "P";
                }
                if (copyByteArray)
                {
                    byte[] byteArray = BitmapImageToByteArray(src);
                    props.AssignByteArray(byteArray);
                }
            }
            catch (NotSupportedException nsex)
            {
                ErrorClass.LogError("GetImageProperties (not supported) - File: " + filePath
                    + Environment.NewLine + nsex.Message);
            }
            catch (OutOfMemoryException omex)
            {
                ErrorClass.LogError("GetImageProperties (outofmemory) - File: " + filePath
                    + Environment.NewLine + omex.Message);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("GetImageProperties - File: " + filePath
                    + Environment.NewLine + ex.Message);
            }
            return props;
        }

        public static ImageProperties GetSizedImageProperties(string filePath, int imageWidth = 0, int imageHeight = 0, Boolean copyByteArray = true)
        {
            ImageProperties props = new ImageProperties();
            BitmapImage src = new BitmapImage();
            try
            {
                src.BeginInit();
                src.DecodePixelWidth = imageWidth;
                src.DecodePixelHeight = imageHeight;
                src.UriSource = new Uri(filePath, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                int lastPosition = filePath.LastIndexOf("\\");
                props.Folder = filePath.Substring(0, lastPosition);
                props.FileName = filePath.Substring(lastPosition + 1);
                props.FileCreateDate = File.GetCreationTime(filePath);
                props.FileModifiedDate = File.GetLastAccessTime(filePath);
                props.FileSize = new FileInfo(filePath).Length;
                props.ImageWidth = Convert.ToInt32(src.DecodePixelWidth);
                props.ImageHeight = Convert.ToInt32(src.DecodePixelHeight);
                props.Diagonal = Math.Sqrt(Math.Pow(src.Width, 2) * Math.Pow(src.Height, 2));
                if (copyByteArray)
                {
                    byte[] byteArray = BitmapImageToByteArray(src);
                    props.AssignByteArray(byteArray);
                }
            }
            catch (NotSupportedException nsex)
            {
                ErrorClass.LogError("GetSizedImageProperties (not supported) - File: " + filePath + " cannot be resized to W: " + imageWidth + " - H: " + imageHeight
                    + Environment.NewLine + nsex.Message);
            }
            catch (OutOfMemoryException omex)
            {
                ErrorClass.LogError("GetSizedImageProperties (outofmemory) - File: " + filePath + " cannot be resized to W: " + imageWidth + " - H: " + imageHeight
                    + Environment.NewLine + omex.Message);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("GetSizedImageProperties - File: " + filePath + " cannot be resized to W: " + imageWidth + " - H: " + imageHeight
                    + Environment.NewLine + ex.Message);
            }
            return props;
        }

        public static Boolean FileContainsImage(string filePath)
        {
            try
            {
                byte[] buffer = File.ReadAllBytes(filePath);
                MemoryStream ms = new MemoryStream(buffer);
                BitmapImage newImage = new BitmapImage();
                newImage.BeginInit();
                newImage.StreamSource = ms;
                newImage.EndInit();
                newImage.Freeze();
            }
            catch (NotSupportedException)
            {
                ErrorClass.LogError("FileContainsImage (notsupported) - File: " + filePath + " does not contain a valid Image");
                return false;
            }
            catch (OutOfMemoryException)
            {
                ErrorClass.LogError("FileContainsImage (outofmemory) - File: " + filePath + "does not contain a valid Image");
                return false;
            }
            catch (Exception)
            {
                ErrorClass.LogError("FileContainsImage (exception) - File: " + filePath + "does not contain a valid Image");
                return false;
            }
            return true;
        }

        public static string CheckImageFileExtension(string filePath, Boolean reName = false)
        {
            string oldExtension;
            string fileNameOnly;
            string newFilePath;
            int lastPosition = filePath.LastIndexOf("\\");
            string folder = filePath.Substring(0, lastPosition);
            string fileName = filePath.Substring(lastPosition + 1);
            lastPosition = fileName.LastIndexOf(".");
            if (lastPosition >= 0)
            {
                oldExtension = fileName.Substring(lastPosition + 1);
                fileNameOnly = fileName.Substring(0, lastPosition);
            }
            else
            {
                oldExtension = "";
                fileNameOnly = fileName;
            }
            string newExtension = GetImageFormatExtension(filePath);
            if (!oldExtension.Equals(newExtension))
            {
                newFilePath = folder + "\\" + fileNameOnly + "." + newExtension;
                if (reName)
                    File.Move(filePath, newFilePath);
            }
            else
            {
                newFilePath = filePath;
            }
            return newFilePath;
        }
        public static string GetImageFormatExtension(string filePath)
        {
            string extension = "";
            Image image = Image.FromFile(filePath);
            if (ImageFormat.Bmp.Equals(image.RawFormat))
                extension = "bmp";
            else if (ImageFormat.Gif.Equals(image.RawFormat))
                extension = "gif";
            else if (ImageFormat.Jpeg.Equals(image.RawFormat))
                extension = "jpg";
            else if (ImageFormat.Png.Equals(image.RawFormat))
                extension = "png";
            else if (ImageFormat.Tiff.Equals(image.RawFormat))
                extension = "tiff";
            else if (ImageFormat.Wmf.Equals(image.RawFormat))
                extension = "wmf";
            else if (ImageFormat.Icon.Equals(image.RawFormat))
                extension = "ico";
            image.Dispose();
            return extension;
        }

    }
}
