IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMovieById'
		)
BEGIN
	DROP PROCEDURE up_GetMovieById
END
GO

CREATE PROCEDURE [dbo].[up_GetMovieById] @MovieId INT
AS
BEGIN
	SELECT *
	FROM Movies
	WHERE MovieID = @MovieId
END