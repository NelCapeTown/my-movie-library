﻿using MyMovieDB_SQL_Data.DataClasses;
using MyMovieLibModels.Models;
using MySqlConnection;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using MyMovieJsonObjects;
using System.Windows.Media.Imaging;


namespace MyMovieLibMVVM.ViewModels
{
    public class FindMoviesViewModel : ViewModelBase
    {
        private ObservableCollection<TmdbMovieModel> _tmdbMovieCollection;
        private String _connectionString;
        private String _tmdbApiKey;
        private TmdbMovieModel _tmdbMovie = new TmdbMovieModel();

        public FindMoviesViewModel()
        {
            _tmdbMovieCollection = new ObservableCollection<TmdbMovieModel>();
            _tmdbMovie = new TmdbMovieModel();
        }

        public String ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public String TmdbApiKey
        {
            get { return _tmdbApiKey; }
            set { _tmdbApiKey = value; }
        }

        private FindMoviesModel findMoviesModel = new FindMoviesModel();

        public FindMoviesModel FindMoviesModel
        {
            get { return findMoviesModel; }
            set { findMoviesModel = value; }
        }

        public void GetTmdbMoviesCollection(string searchString)
        {
            SqlConnClass sqlConn = MySqlConnection.MySqlConnection.GetSqlConnection(_connectionString);
            _tmdbMovieCollection = TmdbMoviesDB.SearchTmdbMovies(sqlConn, searchString);
            sqlConn.CloseConnection();
            if (_tmdbMovieCollection.Count > 0)
                _tmdbMovie = _tmdbMovieCollection[0];
        }

        public ObservableCollection<TmdbMovieModel> TmdbMoviesCollection
        {
            get { return _tmdbMovieCollection; }
        }

        public TmdbMovieModel TmdbMovie
        {
            get { return _tmdbMovie; }
            set
            {
                _tmdbMovie = value;
            }
        }

        public void FetchMovieData(Int32 tmdbId)
        {
            try
            {
                Movies movies = new Movies(_tmdbApiKey);
                Movie movie = movies.GetBasicMovieData(tmdbId);
                findMoviesModel = new FindMoviesModel();
                findMoviesModel.MovieTitle = movie.Title;
                findMoviesModel.TagLine = movie.TagLine;
                findMoviesModel.PosterImage = movies.GetBitmapImage(TmdbImageTypes.Poster, movie.PosterPath);
                findMoviesModel.ReleaseDate = movie.ReleaseDate.ToString("dd MMM yyyy");
                foreach(MyMovieJsonObjects.MovieGenre movieGenre in movie.Genres)
                {
                    MyMovieLibModels.Models.GenreItem genreItem = new GenreItem();
                    genreItem.GenreName = movieGenre.GenreName;
                    findMoviesModel.AddGenre(genreItem);
                }
                MovieCredits movieCredits = movies.GetMovieCredits(tmdbId);
                foreach(MyMovieJsonObjects.CastMember castMember in movieCredits.CastMembers)
                {
                    MyMovieLibModels.Models.CastMember modelCastMember = new MyMovieLibModels.Models.CastMember();
                    modelCastMember.ActorName = castMember.Name;
                    modelCastMember.CharacterName = castMember.Character;
                    modelCastMember.ActorImage = movies.GetBitmapImage(TmdbImageTypes.Profile, castMember.ProfilePath);
                    findMoviesModel.AddCastMember(modelCastMember);
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
