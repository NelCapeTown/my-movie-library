﻿using System;

namespace MyMovieLibModels.Models
{
    public class GenresModel
    {
        public Int32 GenreId { get; set; }
        public String GenreDescription { get; set; }
    }
}
