IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMoviesGenres'
		)
BEGIN
	DROP PROCEDURE up_InsertMoviesGenres
END
GO

CREATE PROCEDURE [dbo].[up_InsertMoviesGenres] @MovieId INT
	,@GenreId INT
AS
BEGIN
	INSERT INTO MoviesGenres (
		MovieId
		,GenreId 
		)
	SELECT @MovieId
		,@GenreId 

END