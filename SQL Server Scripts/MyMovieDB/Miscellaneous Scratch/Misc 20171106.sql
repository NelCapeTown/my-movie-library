SELECT DepartmentDescription, JobDescription, ROW_NUMBER() OVER(PARTITION BY J.DepartmentId ORDER BY J.JobId) AS JobSequence
FROM Jobs J
INNER JOIN Departments D ON J.DepartmentId = D.DepartmentId 
ORDER BY J.DepartmentId, J.JobId 