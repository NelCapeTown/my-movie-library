﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;

namespace MyMovieLibModels.Models
{
    public class CastMember
    {
        public BitmapImage ActorImage { get; set; }
        public String ActorName { get; set; }
        public String CharacterName { get; set; }
    }

    public class GenreItem
    {
        public String GenreName { get; set; }
    }

    public class FindMoviesModel
    {
        private ObservableCollection<CastMember> castList = new ObservableCollection<CastMember>();
        private ObservableCollection<GenreItem> genreList = new ObservableCollection<GenreItem>();
        public String MovieTitle { get; set; }
        public String TagLine { get; set; }
        public BitmapImage PosterImage { get; set; }
        public ObservableCollection<CastMember> CastList { get { return castList; } }
        public ObservableCollection<GenreItem> GenreList { get { return genreList; } }
        public String ReleaseDate { get; set; }

        public void AddCastMember(CastMember castMember)
        {
            castList.Add(castMember);
        }

        public void AddGenre(GenreItem genre)
        {
            genreList.Add(genre);
        }
    }

    public class TmdbMovieModel
    {
        public Int32 TmdbId { get; set; }
        public String MovieTitle { get; set; }
    }
}
