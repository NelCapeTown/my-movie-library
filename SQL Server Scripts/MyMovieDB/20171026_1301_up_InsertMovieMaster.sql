IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMovieMaster'
		)
BEGIN
	DROP PROCEDURE up_InsertMovieMaster
END
GO

  
    
CREATE PROCEDURE [dbo].[up_InsertMovieMaster] @TmdbId INT    
 ,@Title NVARCHAR(400)    
 ,@Popularity DECIMAL(18, 8)    
 ,@IsVideo BIT    
 ,@IsAdult BIT    
AS    
BEGIN    
 INSERT INTO TmdbMovies (    
  TmdbId    
  ,Title    
  ,Popularity    
  ,IsVideo    
  ,IsAdult    
  )    
 SELECT @TmdbId    
  ,@Title    
  ,@Popularity    
  ,@IsVideo    
  ,@IsAdult    
    
 SELECT SCOPE_IDENTITY() AS MovieId    
END