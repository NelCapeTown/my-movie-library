﻿using MyMovieLibModels.Models;
using MySqlConnection;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public static class GenresDB
    {
        public static void InsertGenre(SqlConnClass sqlConnClass, GenresModel genre)
        {
            try
            {
                sqlConnClass.ExecuteNonQuery("up_InsertGenre"
                , genre.GenreId
                , genre.GenreDescription);
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: GenresDB.InsertGenre: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: GenresDB.InsertGenre: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: GenresDB.InsertGenre: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: GenresDB.InsertGenre: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void UpdateGenre(SqlConnClass sqlConnClass, GenresModel genre)
        {
            try
            {
                sqlConnClass.ExecuteNonQuery("up_UpdateGenre"
                , genre.GenreId
                , genre.GenreDescription);
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: GenresDB.UpdateGenre: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: GenresDB.UpdateGenre: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: GenresDB.UpdateGenre: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: GenresDB.UpdateGenre: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static ObservableCollection<GenresModel> GetMyGenres(SqlConnClass sqlConnClass)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetMyLocations");
                ObservableCollection<GenresModel> genres = new ObservableCollection<GenresModel>();
                while (reader.Read())
                {
                    GenresModel genre = ConvertReaderToClass(reader);
                    genres.Add(genre);
                }
                reader.Close();
                return genres;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: GenresDB.GetMyGenres: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: GenresDB.GetMyGenres: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: GenresDB.GetMyGenres: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: GenresDB.GetMyGenres: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static GenresModel GetGenreById(SqlConnClass sqlConnClass, Int32 genreId)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetGenreById", genreId);
                if (reader.Read())
                {
                    GenresModel genre = ConvertReaderToClass(reader);
                    reader.Close();
                    return genre;
                }
                else
                {
                    reader.Close();
                    return null;
                }
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: GenresDB.GetGenreById: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: GenresDB.GetGenreById: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: GenresDB.GetGenreById: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: GenresDB.GetGenreById: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static GenresModel GetGenreByDescription(SqlConnClass sqlConnClass, string genreDescription)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetGenreByDescription", genreDescription);
                if (reader.Read())
                {
                    GenresModel genre = ConvertReaderToClass(reader);
                    reader.Close();
                    return genre;
                }
                reader.Close();
                return null;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: GenresDB.GetGenreByDescription: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: GenresDB.GetGenreByDescription: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: GenresDB.GetGenreByDescription: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: GenresDB.GetGenreByDescription: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private static GenresModel ConvertReaderToClass(SqlDataReader reader)
        {
            try
            {
                GenresModel genre = new GenresModel();
                genre.GenreId = Int32.Parse(reader["GenreId"].ToString());
                genre.GenreDescription = reader["GenreDescription"].ToString();
                return genre;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: GenresDB.ConvertReaderToClass: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: GenresDB.ConvertReaderToClass: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
