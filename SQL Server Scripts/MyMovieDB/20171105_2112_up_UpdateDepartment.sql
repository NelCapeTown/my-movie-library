IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_UpdateDepartment'
		)
BEGIN
	DROP PROCEDURE up_UpdateDepartment
END
GO

CREATE PROCEDURE [dbo].[up_UpdateDepartment] @DepartmentId INT
	,@DepartmentDescription NVARCHAR(200)
AS
BEGIN
	UPDATE Departments
	SET DepartmentDescription = @DepartmentDescription
	WHERE DepartmentId = @DepartmentId
END