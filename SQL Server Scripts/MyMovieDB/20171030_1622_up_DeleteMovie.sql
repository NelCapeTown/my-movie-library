IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_DeleteMovie'
		)
BEGIN
	DROP PROCEDURE up_DeleteMovie
END
GO

CREATE PROCEDURE [dbo].[up_DeleteMovie] @MovieId INT
AS
BEGIN
	DELETE
	FROM Movies
	WHERE MovieID = @MovieId
END