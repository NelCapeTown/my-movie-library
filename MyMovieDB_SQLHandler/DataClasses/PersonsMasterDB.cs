﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MyMovieDB_SQL_Data.Entities;
using MySqlConnection;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public class PersonsMasterDB
    {
        public Int64 InsertPersonMaster(SqlConnClass sqlConnClass, PersonsMaster personsMaster)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertPersonMaster"
                , personsMaster.TmdbId
                , personsMaster.Name
                , personsMaster.Popularity
                , personsMaster.IsAdult);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["PersonId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }

        public Int64 InsertPersonMaster(SqlConnClass sqlConnClass, SqlTransaction sqlTran, PersonsMaster personsMaster)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_InsertPersonMaster"
                , personsMaster.TmdbId
                , personsMaster.Name
                , personsMaster.Popularity
                , personsMaster.IsAdult);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["PersonId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }
    }
}
