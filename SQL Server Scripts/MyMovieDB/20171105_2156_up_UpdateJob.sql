IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_UpdateJob'
		)
BEGIN
	DROP PROCEDURE up_UpdateJob
END
GO

CREATE PROCEDURE [dbo].[up_UpdateJob] @JobId INT
	,@DepartmentId INT
	,@JobDescription NVARCHAR(200)
AS
BEGIN
	UPDATE Jobs
	SET DepartmentId = @DepartmentId
		,JobDescription = @JobDescription
	WHERE JobId = @JobId
END