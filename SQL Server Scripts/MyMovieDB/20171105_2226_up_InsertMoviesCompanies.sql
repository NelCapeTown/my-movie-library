IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMoviesCompanies'
		)
BEGIN
	DROP PROCEDURE up_InsertMoviesCompanies
END
GO

CREATE PROCEDURE [dbo].[up_InsertMoviesCompanies] @MovieId INT
	,@CompanyId INT
AS
BEGIN
	INSERT INTO MoviesCompanies (
		MovieId
		,CompanyId 
		)
	SELECT @MovieId
		,@CompanyId

END