IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertTVSeriesMaster'
		)
BEGIN
	DROP PROCEDURE up_InsertTVSeriesMaster
END
GO

CREATE PROCEDURE [dbo].[up_InsertTVSeriesMaster] @TmdbId INT
	,@Title NVARCHAR(400)
	,@Popularity DECIMAL(18, 8)
AS
BEGIN
	INSERT INTO TmdbSeries (
		TmdbId
		,Title
		,Popularity
		)
	SELECT @TmdbId
		,@Title
		,@Popularity

	SELECT SCOPE_IDENTITY() AS SeriesId
END