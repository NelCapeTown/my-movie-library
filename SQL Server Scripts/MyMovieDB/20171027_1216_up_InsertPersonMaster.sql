IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertPersonMaster'
		)
BEGIN
	DROP PROCEDURE up_InsertPersonMaster
END
GO

  
CREATE PROCEDURE [dbo].[up_InsertPersonMaster] @TmdbId INT  
 ,@Name NVARCHAR(400)  
 ,@Popularity DECIMAL(18, 8)  
 ,@IsAdult BIT  
AS  
BEGIN  
 INSERT INTO TmdbPersons (  
  TmdbId  
  ,[Name]  
  ,Popularity  
  ,IsAdult  
  )  
 SELECT @TmdbId  
  ,@Name  
  ,@Popularity  
  ,@IsAdult  
  
 SELECT SCOPE_IDENTITY() AS PersonId  
END