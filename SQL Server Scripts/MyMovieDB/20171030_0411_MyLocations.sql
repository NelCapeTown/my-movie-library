BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'MyLocations'
		)
BEGIN
CREATE TABLE dbo.MyLocations
	(
	MyLocationId int NOT NULL IDENTITY (1, 1),
	[Description] nvarchar(200) NOT NULL,
	StorageType nvarchar(2) NOT NULL,
	Comments nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
END
GO
ALTER TABLE dbo.MyLocations ADD CONSTRAINT
	DF_MyLocations_StorageType DEFAULT 'D' FOR StorageType
GO
ALTER TABLE dbo.MyLocations ADD CONSTRAINT
	PK_MyLocations PRIMARY KEY CLUSTERED 
	(
	MyLocationId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_MyLocationsDescription ON dbo.MyLocations
	(
	Description
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_MyLocationsStorageTypeDescription ON dbo.MyLocations
	(
	StorageType,
	Description
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.MyLocations SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
