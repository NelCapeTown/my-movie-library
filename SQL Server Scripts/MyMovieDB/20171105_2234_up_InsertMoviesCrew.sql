IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMoviesCrew'
		)
BEGIN
	DROP PROCEDURE up_InsertMoviesCrew
END
GO

CREATE PROCEDURE [dbo].[up_InsertMoviesCrew] @MovieId INT
	,@PersonId INT
	,@DepartmentId INT
	,@JobId INT
	,@Gender NVARCHAR(1)
	,@PersonName NVARCHAR(400)
AS
BEGIN
	INSERT INTO MoviesCrew (
		MovieId
		,PersonId
		,DepartmentId
		,JobId 
		,Gender
		,PersonName
		)
	SELECT @MovieId
		,@PersonId
		,@DepartmentId
		,@JobId 
		,@Gender
		,@PersonName

	SELECT SCOPE_IDENTITY() AS MoviesCrewId
END