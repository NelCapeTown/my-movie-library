IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMoviesGenresByMovieId'
		)
BEGIN
	DROP PROCEDURE up_GetMoviesGenresByMovieId
END
GO

CREATE PROCEDURE [dbo].[up_GetMoviesGenresByMovieId] @MovieId INT
AS
BEGIN
	SELECT *
	FROM MoviesGenres 
	WHERE MovieId = @MovieId 
	ORDER BY GenreId 
END
