BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'TmdbPersons'
		)
BEGIN
	CREATE TABLE dbo.TmdbPersons (
		PersonId INT NOT NULL IDENTITY(1, 1)
		,TmdbId INT NOT NULL
		,[Name] NVARCHAR(500) NOT NULL
		,Popularity DECIMAL(18, 8) NOT NULL
		,IsAdult BIT NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE [parent_object_id] = OBJECT_ID('TmdbPersons', 'U')
			AND [name] = 'DF_TmdbPersons_IsAdult'
		)
BEGIN
	ALTER TABLE dbo.TmdbPersons ADD CONSTRAINT DF_TmdbPersons_IsAdult DEFAULT 0
	FOR IsAdult
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_TmdbPersons'
			AND [object_id] = OBJECT_ID('TmdbPersons', 'U')
		)
BEGIN
	ALTER TABLE dbo.TmdbPersons ADD CONSTRAINT PK_TmdbPersons PRIMARY KEY CLUSTERED (PersonId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbPersonsTmdbId'
			AND [object_id] = OBJECT_ID('TmdbPersons', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_TmdbPersonsTmdbId ON dbo.TmdbPersons (TmdbId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbPersonsTitle'
			AND [object_id] = OBJECT_ID('TmdbPersons', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_TmdbPersonsTitle ON dbo.TmdbPersons (
		[Name]
		,PersonId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.TmdbPersons

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT