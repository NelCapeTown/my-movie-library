﻿using MyMovieDB_SQL_Data.DataClasses;
using MyMovieLibModels.Models;
using MySqlConnection;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;

namespace MyMovieLibMVVM.ViewModels
{
    public class MyLocationsViewModel : ViewModelBase
    {
        private ObservableCollection<MyLocationsModel> _locationsCollection;
        private String _connectionString;
        private MyLocationsModel _location = new MyLocationsModel();

        public MyLocationsViewModel()
        {
            _locationsCollection = new ObservableCollection<MyLocationsModel>();
            _location = new MyLocationsModel();
        }

        public String ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public void GetLocationsCollection()
        {
            SqlConnClass sqlConn = MySqlConnection.MySqlConnection.GetSqlConnection(_connectionString);
            _locationsCollection = MyLocationsDB.GetMyLocations(sqlConn);
            sqlConn.CloseConnection();
            if (_locationsCollection.Count > 0)
                _location = _locationsCollection[0];
        }

        public ObservableCollection<MyLocationsModel> MyLocationsCollection
        {
            get { return _locationsCollection; }
        }

        public MyLocationsModel MyLocation
        {
            get { return _location; }
            set
            {
                _location = value;
            }
        }

        public string JsonLocation
        {
            get { return JsonConvert.SerializeObject(_location); }
        }

        public MyLocationsModel DeserializedModel(string jsonLocation)
        {
            return JsonConvert.DeserializeObject<MyLocationsModel>(jsonLocation);
        }
    }
}
