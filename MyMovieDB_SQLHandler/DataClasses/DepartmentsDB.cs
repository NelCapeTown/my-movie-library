﻿using MyMovieLibModels.Models;
using MySqlConnection;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public static class DepartmentsDB
    {
        public static Int32 InsertDepartment(SqlConnClass sqlConnClass, DepartmentsModel department)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertDepartment"
                , department.DepartmentDescription);
                if (reader.Read())
                {
                    Int32 departmentId = Convert.ToInt32(reader["DepartmentId"]);
                    reader.Close();
                    return departmentId;
                }
                else
                {
                    reader.Close();
                    return 0;
                }
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: DepartmentsDB.InsertDepartment: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: DepartmentsDB.InsertDepartment: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: DepartmentsDB.InsertDepartment: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: DepartmentsDB.InsertDepartment: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
        }

        public static DepartmentsModel GetDepartmentByDescription(SqlConnClass sqlConnClass, string departmentDescription)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetDepartmentByDescription", departmentDescription);
                if (reader.Read())
                {
                    DepartmentsModel department = ConvertReaderToClass(reader);
                    reader.Close();
                    return department;
                }
                reader.Close();
                return null;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: DepartmentsDB.GetDepartmentByDescription: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: DepartmentsDB.GetDepartmentByDescription: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: DepartmentsDB.GetDepartmentByDescription: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: DepartmentsDB.GetDepartmentByDescription: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
        private static DepartmentsModel ConvertReaderToClass(SqlDataReader reader)
        {
            try
            {
                DepartmentsModel department = new DepartmentsModel();
                department.DepartmentId = Int32.Parse(reader["DepartmentId"].ToString());
                department.DepartmentDescription = reader["DepartmentDescription"].ToString();
                return department;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: DepartmentsDB.ConvertReaderToClass: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: DepartmentsDB.ConvertReaderToClass: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
