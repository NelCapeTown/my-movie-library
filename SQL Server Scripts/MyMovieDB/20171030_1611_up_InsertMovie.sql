IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMovie'
		)
BEGIN
	DROP PROCEDURE up_InsertMovie
END
GO

CREATE PROCEDURE [dbo].[up_InsertMovie] @TmdbId INT
	,@ImdbId NVARCHAR(50)
	,@Overview NVARCHAR(MAX)
	,@OriginalLanguage NVARCHAR(30)
	,@Title NVARCHAR(400)
	,@OriginalTitle NVARCHAR(400)
	,@Popularity DECIMAL(18, 8)
	,@ReleaseDate DATETIME
	,@Budget MONEY
	,@Revenue MONEY
	,@Runtime INT
	,@TagLine NVARCHAR(MAX)
	,@IsAdult BIT
	,@IsVideo BIT
	,@VoteAverage DECIMAL(18, 2)
	,@VoteCount INT
AS
BEGIN
	INSERT INTO Movies (
		TmdbId
		,ImdbId
		,Overview
		,OriginalLanguage
		,Title
		,OriginalTitle
		,Popularity
		,ReleaseDate
		,Budget
		,Revenue
		,Runtime
		,TagLine
		,IsAdult
		,IsVideo
		,VoteAverage
		,VoteCount
		)
	SELECT @TmdbId
		,@ImdbId
		,@Overview
		,@OriginalLanguage
		,@Title
		,@OriginalTitle
		,@Popularity
		,@ReleaseDate
		,@Budget
		,@Revenue
		,@Runtime
		,@TagLine
		,@IsAdult
		,@IsVideo
		,@VoteAverage
		,@VoteCount

	SELECT SCOPE_IDENTITY() AS MovieId
END