﻿using MyMovieLibModels.Models;
using MySqlConnection;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public static class MyLocationsDB
    {
        public static Int32 InsertMyLocation(SqlConnClass sqlConnClass, MyLocationsModel location)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertMyLocation"
                , location.Description
                , location.StorageType
                , location.Comments);
                if (reader.Read())
                {
                    Int32 locationId = Convert.ToInt32(reader["MyLocationId"]);
                    reader.Close();
                    return locationId;
                }
                else
                {
                    reader.Close();
                    return 0;
                }
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.InsertMyLocation: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.InsertMyLocation: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.InsertMyLocation: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.InsertMyLocation: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
        }

        public static Int32 InsertMyLocation(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MyLocationsModel location)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_InsertMyLocation"
                    , location.Description
                    , location.StorageType
                    , location.Comments);
                if (reader.Read())
                {
                    Int32 locationId = Convert.ToInt32(reader["MyLocationId"]);
                    reader.Close();
                    return locationId;
                }
                else
                {
                    reader.Close();
                    return 0;
                }
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.InsertMyLocation: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.InsertMyLocation: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.InsertMyLocation: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.InsertMyLocation: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
        }

        public static void UpdateMyLocation(SqlConnClass sqlConnClass, MyLocationsModel location)
        {
            try
            {
                sqlConnClass.ExecuteNonQuery("up_UpdateMyLocation"
                    , location.MyLocationId
                    , location.Description
                    , location.StorageType
                    , location.Comments);
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.UpdateMyLocation: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.UpdateMyLocation: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.UpdateMyLocation: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.UpdateMyLocation: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void UpdateMyLocation(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MyLocationsModel location)
        {
            try
            {
                sqlConnClass.ExecuteNonQuery(sqlTran, "up_UpdateMyLocation"
                    , location.MyLocationId
                    , location.Description
                    , location.StorageType
                    , location.Comments);
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.UpdateMyLocation: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.UpdateMyLocation: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.UpdateMyLocation: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.UpdateMyLocation: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void DeleteMyLocation(SqlConnClass sqlConnClass, MyLocationsModel location)
        {
            try
            {
                sqlConnClass.ExecuteNonQuery("up_DeleteMyLocationById"
                    , location.MyLocationId);
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.DeleteMyLocation: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.DeleteMyLocation: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.DeleteMyLocation: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.DeleteMyLocation: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void DeleteMyLocation(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MyLocationsModel location)
        {
            try
            {
                sqlConnClass.ExecuteNonQuery(sqlTran, "up_DeleteMyLocation"
                    , location.MyLocationId);
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.DeleteMyLocation: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.DeleteMyLocation: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.DeleteMyLocation: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.DeleteMyLocation: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static ObservableCollection<MyLocationsModel> GetMyLocations(SqlConnClass sqlConnClass)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetMyLocations");
                ObservableCollection<MyLocationsModel> locations = new ObservableCollection<MyLocationsModel>();
                while (reader.Read())
                {
                    MyLocationsModel location = ConvertReaderToClass(reader);
                    locations.Add(location);
                }
                reader.Close();
                return locations;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.GetMyLocations: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.GetMyLocations: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.GetMyLocations: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.GetMyLocations: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static ObservableCollection<MyLocationsModel> GetMyLocations(SqlConnClass sqlConnClass, SqlTransaction sqlTran)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_GetMyLocations");
                ObservableCollection<MyLocationsModel> locations = new ObservableCollection<MyLocationsModel>();
                while (reader.Read())
                {
                    MyLocationsModel location = ConvertReaderToClass(reader);
                    locations.Add(location);
                }
                reader.Close();
                return locations;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.GetMyLocations: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.GetMyLocations: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.GetMyLocations: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.GetMyLocations: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static MyLocationsModel GetMyLocationById(SqlConnClass sqlConnClass, Int32 locationId)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetMyLocationById", locationId);
                if (reader.Read())
                {
                    MyLocationsModel location = ConvertReaderToClass(reader);
                    reader.Close();
                    return location;
                }
                else
                {
                    reader.Close();
                    return null;
                }
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.GetMyLocationById: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.GetMyLocationById: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.GetMyLocationById: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.GetMyLocationById: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static MyLocationsModel GetMyLocationById(SqlConnClass sqlConnClass, SqlTransaction sqlTran, Int32 locationId)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_GetMyLocationById", locationId);
                if (reader.Read())
                {
                    MyLocationsModel location = ConvertReaderToClass(reader);
                    reader.Close();
                    return location;
                }
                else
                {
                    reader.Close();
                    return null;
                }

            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: MyLocationsDB.GetMyLocationById: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: MyLocationsDB.GetMyLocationById: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.GetMyLocationById: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.GetMyLocationById: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private static MyLocationsModel ConvertReaderToClass(SqlDataReader reader)
        {
            try
            {
            MyLocationsModel location = new MyLocationsModel();
            location.MyLocationId = Int32.Parse(reader["MyLocationId"].ToString());
            location.Description = reader["Description"].ToString();
            location.StorageType = reader["StorageType"].ToString();
            location.Comments = reader["Comments"].ToString();
            return location;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: MyLocationsDB.ConvertReaderToClass: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: MyLocationsDB.ConvertReaderToClass: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
