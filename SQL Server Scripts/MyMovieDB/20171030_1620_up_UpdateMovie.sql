IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_UpdateMovie'
		)
BEGIN
	DROP PROCEDURE up_UpdateMovie
END
GO

CREATE PROCEDURE [dbo].[up_UpdateMovie] @MovieId INT
	,@TmdbId INT
	,@ImdbId NVARCHAR(50)
	,@Overview NVARCHAR(MAX)
	,@OriginalLanguage NVARCHAR(30)
	,@Title NVARCHAR(400)
	,@OriginalTitle NVARCHAR(400)
	,@Popularity DECIMAL(18, 8)
	,@ReleaseDate DATETIME
	,@Budget MONEY
	,@Revenue MONEY
	,@Runtime INT
	,@TagLine NVARCHAR(MAX)
	,@IsAdult BIT
	,@IsVideo BIT
	,@VoteAverage DECIMAL(18, 2)
	,@VoteCount INT
AS
BEGIN
	UPDATE Movies
	SET TmdbId = @TmdbId
		,ImdbId = @ImdbId
		,Overview = @Overview
		,OriginalLanguage = @OriginalLanguage
		,Title = @Title
		,OriginalTitle = @OriginalTitle
		,Popularity = @Popularity
		,ReleaseDate = @ReleaseDate
		,Budget = @Budget
		,Revenue = @Revenue
		,Runtime = @Runtime
		,TagLine = @TagLine
		,IsAdult = @IsAdult
		,IsVideo = @IsVideo
		,VoteAverage = @VoteAverage
		,VoteCount = @VoteCount
	WHERE MovieID = @MovieId
END