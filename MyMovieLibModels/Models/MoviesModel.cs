﻿using System;
using System.Windows.Controls;

namespace MyMovieLibModels.Models
{
    public class MoviesModel : UserControl 
    {
        public Int32 MovieId { get; set; }
        public Int32 TmdbId { get; set; }
        public String ImdbId { get; set; }
        public String Overview { get; set; }
        public String OriginalLanguage { get; set; }
        public String Title { get; set; }
        public String OriginalTitle { get; set; }
        public double Popularity { get; set; }
        public DateTime ReleaseDate { get; set; }
        public Int32 Budget { get; set; }
        public Int32 Revenue { get; set; }
        public Int32 Runtime { get; set; }
        public String TagLine { get; set; }
        public Boolean IsAdult { get; set; }
        public Boolean IsVideo { get; set; }
        public double VoteAverage { get; set; }
        public Int32 VoteCount { get; set; }
    }
}
