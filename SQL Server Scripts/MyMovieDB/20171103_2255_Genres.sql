BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'Genres'
		)
BEGIN
	CREATE TABLE dbo.Genres (
		GenreId INT NOT NULL
		,GenreDescription NVARCHAR(200) NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_Genres'
			AND [object_id] = OBJECT_ID('Genres', 'U')
		)
BEGIN
	ALTER TABLE dbo.Genres ADD CONSTRAINT PK_Genres PRIMARY KEY CLUSTERED (GenreId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_GenresDescription'
			AND [object_id] = OBJECT_ID('Genres', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_GenresDescription ON dbo.Genres (
		[Description]
		,GenreId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.Genres

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT