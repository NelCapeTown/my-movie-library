﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MyMovieDB_SQL_Data.Entities;
using MySqlConnection;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public class MoviesMasterDB
    {
        public Int64 InsertMovieMaster(SqlConnClass sqlConnClass, MoviesMaster moviesMaster)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertMovieMaster"
                , moviesMaster.TmdbId 
                , moviesMaster.Title 
                , moviesMaster.Popularity 
                , moviesMaster.IsVideo 
                , moviesMaster.IsAdult);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["MovieId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }

        public Int64 InsertMovieMaster(SqlConnClass sqlConnClass, SqlTransaction sqlTran, MoviesMaster moviesMaster)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_InsertMovieMaster"
                , moviesMaster.TmdbId
                , moviesMaster.Title
                , moviesMaster.Popularity
                , moviesMaster.IsVideo
                , moviesMaster.IsAdult);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["MovieId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }

    }
}
