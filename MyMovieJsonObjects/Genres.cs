﻿using MySqlConnection;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace MyMovieJsonObjects
{
    public class GenreList
    {
        [JsonProperty("genres")]
        public List<MovieGenre> Genres { get; set; }
    }

    public class Genres
    {
        private string TmdbApiKey;
        private GenreList _genreList = new GenreList();

        public Genres(string tmdbApiKey)
        {
            TmdbApiKey = tmdbApiKey;
        }

        public GenreList TheGenreList
        {
            get { return _genreList; }
        }

        public GenreList GetGenreData()
        {
            try
            {
                string tmdbUrl = "https://api.themoviedb.org/3/genre/movie/list?api_key=" + TmdbApiKey;
                var client = new RestClient(tmdbUrl);
                var request = new RestRequest(Method.GET);
                request.AddParameter("undefined", "{}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (String.Equals(response.StatusCode.ToString(), "OK"))
                {
                    _genreList = JsonConvert.DeserializeObject<GenreList>(response.Content);
                    return _genreList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Genres.GetGenreData: " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
                throw new Exception("Genres.GetGenreData: " + ex.Message);
            }
        }

    }
}
