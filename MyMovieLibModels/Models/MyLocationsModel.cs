﻿using System;

namespace MyMovieLibModels.Models
{
    public class MyLocationsModel
    {
        public Int32 MyLocationId { get; set; }
        public String Description { get; set; }
        public String StorageType { get; set; }
        public String Comments { get; set; }

        //public MyLocationsModel()
        //{
        //    MyLocationId = 0;
        //    Description = "";
        //    StorageType = "D";
        //    Comments = "";
        //}
    }
}
