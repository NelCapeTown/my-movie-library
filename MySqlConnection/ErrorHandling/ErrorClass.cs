﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MySqlConnection
{
    public static class ErrorClass
    {
        private static string source = "My Movie Library";

        public static void LogError(string entryText, Int32 eventId = 9999, EventLogEntryType entryType = EventLogEntryType.Information, Boolean showStack = false)
        {
            string completeText;
            if (!EventLog.SourceExists(source))
                EventLog.CreateEventSource(source, "Application");
            if (showStack)
            {
                completeText = entryText + Environment.NewLine + Environment.StackTrace;
            }
            else
            {
                completeText = entryText;
            }
            EventLog.WriteEntry(source, completeText, entryType, eventId);
        }
    }
}
