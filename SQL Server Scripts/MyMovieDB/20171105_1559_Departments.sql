BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'Departments'
		)
BEGIN
	CREATE TABLE dbo.Departments (
		DepartmentId INT NOT NULL IDENTITY(1, 1)
		,DepartmentDescription NVARCHAR(200) NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_Departments'
			AND [object_id] = OBJECT_ID('Departments', 'U')
		)
BEGIN
	ALTER TABLE dbo.Departments ADD CONSTRAINT PK_Departments PRIMARY KEY CLUSTERED (DepartmentId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_DepartmentsDescription'
			AND [object_id] = OBJECT_ID('Departments', 'U')
		)
BEGIN
	CREATE NONCLUSTERED INDEX IX_DepartmentsDescription ON dbo.Departments (
		DepartmentDescription
		,DepartmentId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.Departments

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT