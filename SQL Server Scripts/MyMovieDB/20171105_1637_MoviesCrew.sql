BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'MoviesCrew'
		)
BEGIN
	CREATE TABLE dbo.MoviesCrew (
		MoviesCrewId INT NOT NULL IDENTITY(1, 1)
		,MovieId INT NOT NULL
		,PersonId INT NOT NULL
		,DepartmentId INT NOT NULL
		,JobId INT NOT NULL
		,Gender NVARCHAR(1) NOT NULL
		,PersonName NVARCHAR(400) NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE parent_object_id = OBJECT_ID('MoviesCrew', 'U')
			AND [name] = 'DF_MoviesCrew_Gender'
		)
BEGIN
	ALTER TABLE dbo.MoviesCrew ADD CONSTRAINT DF_MoviesCrew_Gender DEFAULT 'U'
	FOR Gender
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_MoviesCrew'
			AND [object_id] = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCrew ADD CONSTRAINT PK_MoviesCrew PRIMARY KEY CLUSTERED (MoviesCrewId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesCrewMovies'
			AND [object_id] = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesCrewMovies ON dbo.MoviesCrew (
		MovieId
		,DepartmentId
		,JobId
		,MoviesCrewId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesCrewDepartment'
			AND [object_id] = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesCrewDepartment ON dbo.MoviesCrew (
		DepartmentId
		,MovieId
		,JobId
		,MoviesCrewId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesCrewJob'
			AND [object_id] = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesCrewJob ON dbo.MoviesCrew (
		JobId
		,MovieId
		,MoviesCrewId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesCrewPerson'
			AND [object_id] = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesCrewPerson ON dbo.MoviesCrew (
		PersonId
		,MovieId
		,DepartmentId
		,JobId
		,MoviesCrewId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Movies', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCrew ADD CONSTRAINT FK_MoviesCrew_Movies FOREIGN KEY (MovieId) REFERENCES dbo.Movies (MovieId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('TmdbPersons', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCrew ADD CONSTRAINT FK_MoviesCrew_Persons FOREIGN KEY (PersonId) REFERENCES dbo.TmdbPersons (PersonId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Departments', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCrew ADD CONSTRAINT FK_MoviesCrew_Departments FOREIGN KEY (DepartmentId) REFERENCES dbo.Departments (DepartmentId) ON

	UPDATE NO ACTION ON

	DELETE NO ACTION
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Jobs', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCrew', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCrew ADD CONSTRAINT FK_MoviesCrew_Jobs FOREIGN KEY (JobId) REFERENCES dbo.Jobs (JobId) ON

	UPDATE NO ACTION ON

	DELETE NO ACTION
END
GO

ALTER TABLE dbo.MoviesCrew

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT