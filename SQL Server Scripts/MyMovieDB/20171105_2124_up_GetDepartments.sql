IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetDepartments'
		)
BEGIN
	DROP PROCEDURE up_GetDepartments
END
GO

CREATE PROCEDURE [dbo].[up_GetDepartments] 
AS
BEGIN
	SELECT *
	FROM Departments
	ORDER BY DepartmentId

END