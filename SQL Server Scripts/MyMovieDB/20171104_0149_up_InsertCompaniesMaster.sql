IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertCompaniesMaster'
		)
BEGIN
	DROP PROCEDURE up_InsertCompaniesMaster
END
GO

CREATE PROCEDURE [dbo].[up_InsertCompaniesMaster] @TmdbId INT
	,@Title NVARCHAR(400)
AS
BEGIN
	INSERT INTO TmdbCompanies (
		TmdbId
		,Title
		)
	SELECT @TmdbId
		,@Title

	SELECT SCOPE_IDENTITY() AS CompanyId
END