IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertDepartment'
		)
BEGIN
	DROP PROCEDURE up_InsertDepartment
END
GO

CREATE PROCEDURE [dbo].[up_InsertDepartment] @DepartmentDescription NVARCHAR(200)
AS
BEGIN
	INSERT INTO Departments (DepartmentDescription)
	SELECT @DepartmentDescription

	SELECT SCOPE_IDENTITY() AS DepartmentId
END