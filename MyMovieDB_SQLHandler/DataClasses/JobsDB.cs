﻿using MyMovieLibModels.Models;
using MySqlConnection;
using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;


namespace MyMovieDB_SQL_Data.DataClasses
{
    public static class JobsDB
    {
        public static Int32 InsertJob(SqlConnClass sqlConnClass, JobsModel job)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertJob", job.DepartmentId 
                , job.JobDescription);
                if (reader.Read())
                {
                    Int32 departmentId = Convert.ToInt32(reader["JobId"]);
                    reader.Close();
                    return departmentId;
                }
                else
                {
                    reader.Close();
                    return 0;
                }
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: JobsDB.InsertJob: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: JobsDB.InsertJob: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: JobsDB.InsertJob: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: JobsDB.InsertJob: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
        }

        public static JobsModel GetJobByDescription(SqlConnClass sqlConnClass, JobsModel job)
        {
            try
            {
                SqlDataReader reader = sqlConnClass.ExecuteReader("up_GetJobByDescription", job.DepartmentId, job.JobDescription);
                if (reader.Read())
                {
                    JobsModel _job = ConvertReaderToClass(reader);
                    reader.Close();
                    return _job;
                }
                reader.Close();
                return null;
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error: JobsDB.GetJobByDescription: " + sqlex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("SQL Error: JobsDB.GetJobByDescription: " + sqlex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: JobsDB.GetJobByDescription: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: JobsDB.GetJobByDescription: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
        private static JobsModel ConvertReaderToClass(SqlDataReader reader)
        {
            try
            {
                JobsModel _job = new JobsModel();
                _job.JobId = Int32.Parse(reader["JobId"].ToString());
                _job.DepartmentId = Int32.Parse(reader["DepartmentId"].ToString());
                _job.JobDescription = reader["JobDescription"].ToString();
                return _job;
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Error: JobsDB.ConvertReaderToClass: " + ex.Message, 9998, System.Diagnostics.EventLogEntryType.Error, true);
                MessageBox.Show("Error: JobsDB.ConvertReaderToClass: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
