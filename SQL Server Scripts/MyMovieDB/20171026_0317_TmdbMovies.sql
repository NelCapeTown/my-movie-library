BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'TmdbMovies'
		)
BEGIN
	CREATE TABLE dbo.TmdbMovies (
		TmdbMovieId INT NOT NULL IDENTITY(1, 1)
		,TmdbId INT NOT NULL
		,Title NVARCHAR(200) NOT NULL
		,Popularity DECIMAL(18, 8) NOT NULL
		,IsVideo BIT NOT NULL
		,IsAdult BIT NOT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE [parent_object_id] = OBJECT_ID('TmdbMovies', 'U')
			AND [name] = 'DF_TmdbMovies_IsVideo'
		)
BEGIN
	ALTER TABLE dbo.TmdbMovies ADD CONSTRAINT DF_TmdbMovies_IsVideo DEFAULT 0
	FOR IsVideo
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE [parent_object_id] = OBJECT_ID('TmdbMovies', 'U')
			AND [name] = 'DF_TmdbMovies_IsAdult'
		)
BEGIN
	ALTER TABLE dbo.TmdbMovies ADD CONSTRAINT DF_TmdbMovies_IsAdult DEFAULT 0
	FOR IsAdult
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_TmdbMovies'
			AND [object_id] = OBJECT_ID('TmdbMovies', 'U')
		)
BEGIN
	ALTER TABLE dbo.TmdbMovies ADD CONSTRAINT PK_TmdbMovies PRIMARY KEY CLUSTERED (TmdbMovieId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbMoviesTmdbId'
			AND [object_id] = OBJECT_ID('TmdbMovies', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_TmdbMoviesTmdbId ON dbo.TmdbMovies (TmdbId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_TmdbMoviesTmdbId'
			AND [object_id] = OBJECT_ID('TmdbMovies', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_TmdbMoviesTitle ON dbo.TmdbMovies (
		Title
		,TmdbMovieId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

ALTER TABLE dbo.TmdbMovies

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT