IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMoviesCast'
		)
BEGIN
	DROP PROCEDURE up_InsertMoviesCast
END
GO

CREATE PROCEDURE [dbo].[up_InsertMoviesCast] @MovieId INT
	,@CharacterName NVARCHAR(200)
	,@CreditId NVARCHAR(100)
	,@Gender NVARCHAR(1)
	,@PersonId INT
	,@PersonName NVARCHAR(400)
	,@CastOrder INT
AS
BEGIN
	INSERT INTO MoviesCast (
		MovieId
		,CharacterName
		,CreditId
		,Gender
		,PersonId
		,PersonName
		,CastOrder
		)
	SELECT @MovieId
		,@CharacterName
		,@CreditId
		,@Gender
		,@PersonId
		,@PersonName
		,@CastOrder

	SELECT SCOPE_IDENTITY() AS MoviesCastId
END