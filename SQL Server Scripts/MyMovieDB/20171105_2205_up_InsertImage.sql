IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertImage'
		)
BEGIN
	DROP PROCEDURE up_InsertImage
END
GO

CREATE PROCEDURE [dbo].[up_InsertImage] @ImageContent VARBINARY(MAX)
AS
BEGIN
	INSERT INTO Images (ImageContent)
	SELECT @ImageContent

	SELECT SCOPE_IDENTITY() AS ImageId
END
