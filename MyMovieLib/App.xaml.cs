﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using MySqlConnection;

namespace MyMovieLib
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string _tmdbApiKey;
        public static string _connectionString;
        public static SqlConnClass _sqlConnClass;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            _connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            _tmdbApiKey = ConfigurationManager.AppSettings["TmdbApiKey"];
            _sqlConnClass = new SqlConnClass(_connectionString);
        }

    }
}
