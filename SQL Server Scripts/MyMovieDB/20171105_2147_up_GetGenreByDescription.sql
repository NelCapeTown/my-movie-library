IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetGenreByDescription'
		)
BEGIN
	DROP PROCEDURE up_GetGenreByDescription
END
GO

CREATE PROCEDURE [dbo].[up_GetGenreByDescription] @GenreDescription NVARCHAR(200)
AS
BEGIN
	SELECT *
	FROM Genres
	WHERE GenreDescription = @GenreDescription

END
