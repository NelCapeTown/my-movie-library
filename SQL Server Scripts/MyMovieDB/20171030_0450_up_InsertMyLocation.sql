IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_InsertMyLocation'
		)
BEGIN
	DROP PROCEDURE up_InsertMyLocation
END
GO

CREATE PROCEDURE [dbo].[up_InsertMyLocation] @Description NVARCHAR(200)
	,@StorageType NVARCHAR(2)
	,@Comments NVARCHAR(MAX)
AS
BEGIN
	INSERT INTO MyLocations (
		[Description]
		,StorageType
		,Comments
		)
	SELECT @Description
		,@StorageType
		,@Comments

	SELECT SCOPE_IDENTITY() AS MyLocationID
END