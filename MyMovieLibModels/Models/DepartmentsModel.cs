﻿using System;

namespace MyMovieLibModels.Models
{
    public class DepartmentsModel
    {
        public Int32 DepartmentId { get; set; }
        public String DepartmentDescription { get; set; }
    }
}
