IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetDepartmentByDescription'
		)
BEGIN
	DROP PROCEDURE up_GetDepartmentByDescription
END
GO

CREATE PROCEDURE [dbo].[up_GetDepartmentByDescription] @DepartmentDescription NVARCHAR(200)
AS
BEGIN
	SELECT *
	FROM Departments
	WHERE DepartmentDescription = @DepartmentDescription

END