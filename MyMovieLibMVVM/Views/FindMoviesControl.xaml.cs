﻿using MyMovieLibModels.Models;
using MyMovieLibMVVM.ViewModels;
using MySqlConnection;
using MyMovieDB_SQL_Data.DataClasses;
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace MyMovieLibMVVM.Views
{
    /// <summary>
    /// Interaction logic for FindMoviesControl.xaml
    /// </summary>
    public partial class FindMoviesControl : UserControl
    {
        public static readonly RoutedEvent StatusBarChangedEvent = EventManager.RegisterRoutedEvent(
            "ChangeStatusText", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(FindMoviesControl));

        public static readonly DependencyProperty StatusTextProperty = DependencyProperty.Register("StatusText", typeof(string), typeof(FindMoviesControl));

        public string StatusText
        {
            get { return (string)GetValue(StatusTextProperty); }
            set
            {
                SetValue(StatusTextProperty, value);
            }
        }

        public event RoutedEventHandler ChangeStatusText
        {
            add { AddHandler(StatusBarChangedEvent, value); }
            remove { RemoveHandler(StatusBarChangedEvent, value); }
        }

        void RaiseStatusBarChangeEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(FindMoviesControl.StatusBarChangedEvent);
            RaiseEvent(newEventArgs);
        }

        private void UpdateStatusBarText(string newValue)
        {
            StatusText = newValue;
            RaiseStatusBarChangeEvent();
        }

        private FindMoviesViewModel viewModel;
        private String _connectionString;
        private String _tmdbApiKey;

        public String ConnectionString
        {
            get { return _connectionString; }
            set
            {
                _connectionString = value;
                viewModel.ConnectionString = _connectionString;
            }
        }

        public String TmdbApiKey
        {
            get { return _tmdbApiKey; }
            set
            {
                _tmdbApiKey = value;
                viewModel.TmdbApiKey = _tmdbApiKey;
            }
        }

        public FindMoviesControl()
        {
            InitializeComponent();
            viewModel = new FindMoviesViewModel();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (txtSearchString.Text.Length > 1)
            {
                viewModel.GetTmdbMoviesCollection(txtSearchString.Text);
                lstMovies.ItemsSource = viewModel.TmdbMoviesCollection;
            }
            else
            {
                MessageBox.Show("Please enter at least a 2 character search string before searching", "Validation Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void lstMovies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstMovies.SelectedItem != null)
            {
                TmdbMovieModel tmdb = (TmdbMovieModel)lstMovies.SelectedItem;
                viewModel.FetchMovieData(tmdb.TmdbId);
                UpdateStatusBarText("Movie : " + viewModel.FindMoviesModel.MovieTitle);
                DataContext = viewModel.FindMoviesModel;
                CastList.ItemsSource = viewModel.FindMoviesModel.CastList;
                GenreList.ItemsSource = viewModel.FindMoviesModel.GenreList;
            }
            else
            {
                if (viewModel.TmdbMoviesCollection.Count > 0)
                {
                    lstMovies.SelectedIndex = 0;
                    TmdbMovieModel tmdb = (TmdbMovieModel)lstMovies.SelectedItem;
                    viewModel.FetchMovieData(tmdb.TmdbId);
                    UpdateStatusBarText("Movie : " + viewModel.FindMoviesModel.MovieTitle);
                    DataContext = viewModel.FindMoviesModel;
                }
            }
        }

    }
}
