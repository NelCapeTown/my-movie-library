﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MyMovieDB_SQL_Data.Entities;
using MySqlConnection;

namespace MyMovieDB_SQL_Data.DataClasses
{
    public class TVSeriesMasterDB
    {
        public Int64 InsertTVSeriesMaster(SqlConnClass sqlConnClass, TVSeriesMaster tvSeriesMaster)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader("up_InsertTVSeriesMaster"
                , tvSeriesMaster.TmdbId
                , tvSeriesMaster.Title
                , tvSeriesMaster.Popularity);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["SeriesId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }

        public Int64 InsertTVSeriesMaster(SqlConnClass sqlConnClass, SqlTransaction sqlTran, TVSeriesMaster tvSeriesMaster)
        {
            SqlDataReader reader = sqlConnClass.ExecuteReader(sqlTran, "up_InsertTVSeriesMaster"
                , tvSeriesMaster.TmdbId
                , tvSeriesMaster.Title
                , tvSeriesMaster.Popularity);
            if (reader.Read())
            {
                Int64 movieId = Convert.ToInt64(reader["SeriesId"]);
                reader.Close();
                return movieId;
            }
            else
            {
                reader.Close();
                return 0;
            }
        }
    }
}
