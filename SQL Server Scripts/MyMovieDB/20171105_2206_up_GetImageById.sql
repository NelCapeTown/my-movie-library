IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetImageById'
		)
BEGIN
	DROP PROCEDURE up_GetImageById
END
GO

CREATE PROCEDURE [dbo].[up_GetImageById] @ImageId INT
AS
BEGIN
	SELECT *
	FROM Images
	WHERE ImageId = @ImageId

END