IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_SearchTmdbMovies'
		)
BEGIN
	DROP PROCEDURE up_SearchTmdbMovies
END
GO

CREATE PROCEDURE up_SearchTmdbMovies @SearchString NVARCHAR(200)
AS
BEGIN
DECLARE @LikeString NVARCHAR(202)
SET @LikeString = '%' + @SearchString + '%'
SELECT *
FROM (
	SELECT TOP 400 TmdbId, Title
	FROM TmdbMovies
	WHERE Title LIKE @LikeString 
		AND IsAdult = 0
	ORDER BY Popularity DESC
	) A
ORDER BY Title
END

