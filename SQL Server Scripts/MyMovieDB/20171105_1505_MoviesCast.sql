BEGIN TRANSACTION

SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

COMMIT

BEGIN TRANSACTION
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE [type] = 'U'
			AND [name] = 'MoviesCast'
		)
BEGIN
	CREATE TABLE dbo.MoviesCast (
		MoviesCastId INT NOT NULL IDENTITY(1, 1)
		,MovieId INT NOT NULL
		,CharacterName NVARCHAR(200) NULL
		,CreditId NVARCHAR(100) NULL
		,Gender NVARCHAR(1) NOT NULL
		,PersonId INT NOT NULL
		,PersonName NVARCHAR(400) NULL
		,CastOrder INT NULL
		) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.default_constraints
		WHERE parent_object_id = OBJECT_ID('MoviesCast', 'U')
			AND [name] = 'DF_MoviesCast_Gender'
		)
BEGIN
	ALTER TABLE dbo.MoviesCast ADD CONSTRAINT DF_MoviesCast_Gender DEFAULT 'U'
	FOR Gender
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'PK_MoviesCast'
			AND [object_id] = OBJECT_ID('MoviesCast', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCast ADD CONSTRAINT PK_MoviesCast PRIMARY KEY CLUSTERED (MoviesCastId)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesCastMovies'
			AND [object_id] = OBJECT_ID('MoviesCast', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesCastMovies ON dbo.MoviesCast (
		MovieId
		,PersonId
		,MoviesCastId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes
		WHERE [name] = 'IX_MoviesCastPerson'
			AND [object_id] = OBJECT_ID('MoviesCast', 'U')
		)
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX IX_MoviesCastPerson ON dbo.MoviesCast (
		PersonId
		,MovieId
		,MoviesCastId
		)
		WITH (
				STATISTICS_NORECOMPUTE = OFF
				,IGNORE_DUP_KEY = OFF
				,ALLOW_ROW_LOCKS = ON
				,ALLOW_PAGE_LOCKS = ON
				) ON [PRIMARY]
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('Movies', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCast', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCast ADD CONSTRAINT FK_MoviesCast_Movies FOREIGN KEY (MovieId) REFERENCES dbo.Movies (MovieId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

IF NOT EXISTS (
		SELECT 1
		FROM sys.foreign_keys
		WHERE referenced_object_id = OBJECT_ID('TmdbPersons', 'U')
			AND parent_object_id = OBJECT_ID('MoviesCast', 'U')
		)
BEGIN
	ALTER TABLE dbo.MoviesCast ADD CONSTRAINT FK_MoviesCast_Persons FOREIGN KEY (PersonId) REFERENCES dbo.TmdbPersons (PersonId) ON

	UPDATE CASCADE ON

	DELETE CASCADE
END
GO

ALTER TABLE dbo.MoviesCast

SET (LOCK_ESCALATION = TABLE)
GO

COMMIT
