IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_DeleteMyLocationById'
		)
BEGIN
	DROP PROCEDURE up_DeleteMyLocationById
END
GO

CREATE PROCEDURE [dbo].[up_DeleteMyLocationById] 
@MyLocationId INT
AS
BEGIN
	DELETE
	FROM MyLocations
	WHERE MyLocationId = @MyLocationId 
END