﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace MySqlConnection 
{
    public class SqlConnClass
    {
        private SqlConnection _conn;
        private string _connectionString;

        public SqlConnClass(string connectionString)
        {
            try
            {
            _connectionString = connectionString;
            _conn = new SqlConnection(_connectionString);
            _conn.Open();
            }
            catch (SqlException sqlex)
            {
                ErrorClass.LogError("SQL Error - SqlConnClass initialise - " + sqlex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
            }
            catch (Exception ex)
            {
                ErrorClass.LogError("Exception - SqlConnClass initialise - " + ex.Message, 9999, System.Diagnostics.EventLogEntryType.Error, true);
            }
        }

        public void CloseConnection()
        {
            _conn.Close();
        }

        public SqlTransaction BeginTransaction()
        {
            if (_conn.State != System.Data.ConnectionState.Open)
            {
                _conn.Close();
                _conn.Open();
            }
            return _conn.BeginTransaction();
        }

        public SqlTransaction BeginTransaction(string transactionName)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
            {
                _conn.Close();
                _conn.Open();
            }
            return _conn.BeginTransaction(transactionName);
        }

        public int ExecuteNonQuery(string spName, params object[] parameterValues)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
            {
                _conn.Close();
                _conn.Open();
            }
            return SqlHelper.ExecuteNonQuery(_conn, spName, parameterValues);
        }

        public int ExecuteNonQuery(SqlTransaction transaction, string spName, params object[] parameterValues)
        {
            return SqlHelper.ExecuteNonQuery(transaction, spName, parameterValues);
        }

        public SqlDataReader ExecuteReader(string spName, params object[] parameterValues)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
            {
                _conn.Close();
                _conn.Open();
            }
            return SqlHelper.ExecuteReader(_conn, spName, parameterValues);
        }

        public SqlDataReader ExecuteReader(SqlTransaction transaction, string spName, params object[] parameterValues)
        {
            return SqlHelper.ExecuteReader(transaction, spName, parameterValues);
        }
    }
}
