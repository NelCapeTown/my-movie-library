﻿using System;

namespace MyMovieLibModels.Models
{
    public class JobsModel
    {
        public Int32 JobId { get; set; }
        public Int32 DepartmentId { get; set; }
        public String JobDescription { get; set; }
    }
}
