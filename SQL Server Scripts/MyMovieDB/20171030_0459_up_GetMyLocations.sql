IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMyLocations'
		)
BEGIN
	DROP PROCEDURE up_GetMyLocations
END
GO

CREATE PROCEDURE [dbo].[up_GetMyLocations] 
AS
BEGIN
	SELECT *
	FROM MyLocations
	ORDER BY MyLocationId
END