IF EXISTS (
		SELECT 1
		FROM sys.objects
		WHERE type = 'P'
			AND NAME = 'up_GetMyLocationById'
		)
BEGIN
	DROP PROCEDURE up_GetMyLocationById
END
GO

CREATE PROCEDURE [dbo].[up_GetMyLocationById] 
@MyLocationId INT
AS
BEGIN
	SELECT *
	FROM MyLocations
	WHERE MyLocationId = @MyLocationId 
END